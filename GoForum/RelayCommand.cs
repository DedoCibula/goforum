﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GoForum
{
    public class RelayCommand : ICommand
    {
        private readonly Action _command;
        private bool _canExecute = true;

        public event EventHandler CanExecuteChanged;

        public bool Executable
        {
            get { return _canExecute; }
            set
            {
                if (_canExecute == value) return;
                _canExecute = value;
                var handler = CanExecuteChanged;
                if (handler != null) 
                    handler(this, EventArgs.Empty);
            }
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public void Execute(object parameter)
        {
            _command();
        }

        public RelayCommand(Action command)
        {
            _command = command;
        }
    }
}
