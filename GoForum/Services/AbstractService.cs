﻿using System;

using GoForum.Models.Tapatalk_Structures;
using GoForum.Services.Tapatalk_API;

using Ninject;
using System.Text;

namespace GoForum.Services
{
    public abstract class AbstractService
    {
        private readonly string _url;
        private readonly IDisposable _configurationObserver;
        private TapatalkProxy _proxy;

        protected Configuration Configuration;

        [Inject]
        public TimeoutMonitor Monitor { get; set; }


        [Inject]
        public TapatalkProxy Proxy
        {
            get { return _proxy; }
            set 
            { 
                _proxy = value;
                _proxy.Url = _url;

                if (App.IsLoggedIn(_url))
                    Proxy.SetCookies(App.Cookies[_url]);
            }
        }

        protected AbstractService(string url)
        {
            if (url == null)
                throw new ArgumentNullException("url");
            _url = url;

            if (App.Configurations[_url] != null)
                Configuration = App.Configurations[_url];
            else
            {
                _configurationObserver = Events<Configuration>.Subscribe(c =>
                    {
                        if (c.Url == _url)
                            Configuration = c;
                    }
                    ,
                    error => 
                    { 
                    
                    });
            }

            // Subscription to Login event here
        }


        public virtual void Dispose(params IDisposable[] subscribers)
        {
            Proxy = null;
            Events<Configuration>.Dispose(_configurationObserver);

            // Disposal of subscription here
        }
    }
}
