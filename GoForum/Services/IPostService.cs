﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GoForum.Models.Tapatalk_Structures;


namespace GoForum.Services
{
    public interface IPostService
    {
        void ReplyPost(int forumId, int topicId, string subject, string textBody, int[] attachementIds = null, int? groupId = null, bool? returnHtml = null);
        /// <summary>
        /// Returns Thread. Max 50 posts.
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="startNum"></param>
        /// <param name="lastNum"></param>
        /// <param name="returnHtml"></param>
        void GetThread(int? topicId, int startNum, int lastNum, bool? returnHtml);
    }
}
