﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoForum.Models.Tapatalk_Structures;


namespace GoForum.Services
{
    public interface IForumService
    {
        void GetConfiguration();
        void GetForums(bool isRequired = false, int? forumId = null);
        void LoginForum(int forumId, string password);
        void GetBoardStatistics();
        void MarkAllAsRead(int? forumId = null);
        void GetForumStatus(params int[] forumIds);
    }
}
