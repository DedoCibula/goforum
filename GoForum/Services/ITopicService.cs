﻿using GoForum.Models.Tapatalk_Structures;
using GoForum.Models.Tapatalk_Structures.InnerStructures;


namespace GoForum.Services
{
    public interface ITopicService
    {
        void NewTopic(int forumId, string subject, string textBody, int? prefixId, int[] attachementIds, int? groupId);
        void GetTopic(int forumId, int? startNum, int? lastNum, TopicMode mode);
        void GetSubscribedTopics(int startNum, int lastNum);
        void SubscribeTopic(int topicID);
        void UnsubscribeTopic(int topicID);
    }
}
