﻿using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using GoForum.Models;

namespace GoForum.Services
{
    public class FavoriteForumProvider
    {
        private List<FavoriteForum> _forums;

        private IsolatedStorageSettings _storage;

        public int Count { get { return _forums.Count; } }

        public FavoriteForumProvider()
        {
            _storage = IsolatedStorageSettings.ApplicationSettings;
            if (!_storage.Contains("favoriteForums") || _storage["favoriteForums"] == null)
                _storage.Add("favoriteForums", new List<FavoriteForum>());

            _forums = (List<FavoriteForum>)_storage["favoriteForums"];
        }

        public void Add(FavoriteForum forum)
        {
            _forums.Add(forum);
            _storage.Save();
        }

        public void Remove(FavoriteForum forum)
        {
            _forums.Remove(forum);
            _storage.Save();
        }

        public FavoriteForum FindByMobiquoUrl(string url)
        {
            return _forums.FirstOrDefault(forum => forum.MobiquoUrl == url);
        }

        public void Clear()
        {
            _forums.Clear();
        }

        public IEnumerator<FavoriteForum> GetEnumerator()
        {
            return _forums.GetEnumerator();
        }

        public IEnumerable<FavoriteForum> GetForums()
        {
            return _forums.ToList();
        }

        public void Update(FavoriteForum forum)
        {
            _forums.Remove(forum);
            _forums.Add(forum);
            _storage.Save();
        }
    }
}