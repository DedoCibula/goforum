﻿using System;
using System.Net;

namespace GoForum.Services.Forum
{
    class MobiquoDiscovery
    {
        public string CanonizeUrl(string url)
        {
            if (!(url.Contains("http://") || url.Contains("https://")))
                url = "http://" + url;

            if (!url.EndsWith("/"))
                url = url + "/";

            try
            {
                var request = WebRequest.Create(url);
                var response = request.GetResponse(TimeSpan.FromSeconds(10));
                if (response == null)
                    throw new CannonizationException();

                return response.ResponseUri.ToString();
            }
            catch (WebException)
            {
                throw new CannonizationException();
            }
        }

        public bool CheckIfForumHasPlugin(string url, bool alreadyCanonized)
        {
            try
            {
                var cannonizedUrl = url;
                if (!alreadyCanonized)
                {
                    cannonizedUrl = CanonizeUrl(url);
                }


                var request = WebRequest.Create(cannonizedUrl + "mobiquo/mobiquo.php");
                var response = request.GetResponse(TimeSpan.FromSeconds(10));

                if (response == null)
                    return false;
            }
            catch (CannonizationException)
            {
                return false;
            }
            catch (WebException)
            {
                return false;
            }

            return true;
        }

        public bool CheckIfForumHasPlugin(string url)
        {
            return CheckIfForumHasPlugin(url, false);
        }
    }
}
