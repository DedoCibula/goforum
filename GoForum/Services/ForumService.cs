﻿using System;
using GoForum.Models.Tapatalk_Structures;

namespace GoForum.Services
{
    public class ForumService : AbstractService, IForumService
    {
        public ForumService(string url) : base(url)
        {
        }

        public void GetConfiguration()
        {
            Proxy.SetCookies(App.Cookies[Proxy.Url, true]);

            Proxy.BeginGetConfiguration(result =>
                {
                    try
                    {
                        var configuration = Proxy.EndGetConfiguration(result);
                        if (!configuration.Result)
                            throw new Exception(configuration.ResultText);
                        configuration.Url = Proxy.Url;
                        Events<Configuration>.OnNext(configuration);
                    }
                    catch (Exception e) // some exception
                    {
                        Events<Configuration>.OnError(e);
                    }
                });
        }

        public void GetForums(bool returnDescription = true, int? forumId = null)
        {
            if (Configuration != null && Configuration.Function_GetForum_Available != null && Configuration.Function_GetForum_Available != "1")
            {
                Events<ForumObj[]>.OnError(new NotSupportedException(Proxy.Url + " doesn't support GetForums method"));
                return;
            }

            if (forumId.HasValue && forumId < 0)
            {
                Events<ForumObj[]>.OnError(new ArgumentException("forumId cannot be negative"));
                return;
            }

            Monitor.Acquire(TimeSpan.FromSeconds(200));

            Proxy.SetCookies(App.Cookies[Proxy.Url]);

            Proxy.BeginGetForum(returnDescription, forumId, result =>
                {
                    try
                    {
                        var forums = Proxy.EndGetForum(result);
                        if (forums.Length == 0)
                            throw new ArgumentException("forumId doesn't exist");
                        Events<ForumObj[]>.OnNext(forums);
                    }
                    catch (Exception e)
                    {
                        Events<ForumObj[]>.OnError(e);
                    }
                    finally
                    {
                        Monitor.Release();
                    }
                });
        }

        public void LoginForum(int forumId, string password)
        {
            if (password == null)
            { 
                Events<LoginForum>.OnError(new ArgumentNullException("password"));
                return;
            }
            if (forumId < 0)
            {
                Events<LoginForum>.OnError(new ArgumentException("forumId cannot be negative"));
                return;
            }

            Proxy.SetCookies(App.Cookies[Proxy.Url, true]);

            Proxy.BeginLoginForum(forumId, password, result =>
                {
                    try
                    {
                        var loginForum = Proxy.EndLoginForum(result);
                        if (!loginForum.Result)
                            throw new Exception(loginForum.ResultText);
                        Events<LoginForum>.OnNext(loginForum);
                    }
                    catch (Exception e)
                    {
                        Events<LoginForum>.OnError(e);
                    }
                });
        }

        public void GetBoardStatistics()
        {
            Proxy.SetCookies(App.Cookies[Proxy.Url, true]);

            Proxy.BeginGetBoardStatistics(result =>
                {
                    try
                    {
                        var statistics = Proxy.EndGetBoardStatistics(result);
                        if (!statistics.Result)
                            throw new Exception(statistics.ResultText);
                        Events<BoardStats>.OnNext(statistics);
                    }
                    catch (Exception e)
                    {
                        Events<BoardStats>.OnError(e);
                    }
                });
        }

        public void MarkAllAsRead(int? forumId = null)
        {
            if (forumId.HasValue)
            {
                if (Configuration == null || Configuration.MarkForumAvailable == "0")
                {
                    Events<MarkAllAsRead>.OnError(new ArgumentException("MarkAllAsRead with forumId isn't available in this forum"));
                    return;
                }
            }

            Proxy.SetCookies(App.Cookies[Proxy.Url, true]);

            Proxy.BeginMarkAllAsRead(forumId, result =>
                {
                    try
                    {
                        var markResult = Proxy.EndMarkAllAsRead(result);
                        if (!markResult.Result)
                            throw new Exception(markResult.ResultText);
                        Events<MarkAllAsRead>.OnNext(markResult);
                    }
                    catch (Exception e)
                    {
                        Events<MarkAllAsRead>.OnError(e);
                    }
                });
        }

        public void GetForumStatus(params int[] forumIds)
        {
            if (Configuration == null || Configuration.Function_GetForumStatus_Available == null ||
                Configuration.Function_GetForumStatus_Available == "0")
            {
                Events<ForumStatus>.OnError(
                    new NotSupportedException(Proxy.Url + " doesn't support GetForumStatus method"));
                return;
            }

            if (forumIds == null)
            { 
                Events<ForumStatus>.OnError(new ArgumentNullException("forumIds"));
                return;
            }
            if (forumIds.Length == 0)
            {
                Events<ForumStatus>.OnError(new ArgumentException("Array with forum ids cannot be empty"));
                return;
            }

            Proxy.SetCookies(App.Cookies[Proxy.Url, true]);

            Proxy.BeginGetForumStatus(forumIds, result =>
                {
                    try
                    {
                        var forumStatus = Proxy.EndGetForumStatus(result);
                        if (!forumStatus.Result)
                            throw new Exception(forumStatus.ResultText);
                        Events<ForumStatus>.OnNext(forumStatus);
                    }
                    catch (Exception e)
                    {
                        Events<ForumStatus>.OnError(e);
                    }
                });
        }

        public override void Dispose(params IDisposable[] subscribers)
        {
            base.Dispose();
            Events<Configuration>.Dispose(subscribers);
            Events<ForumObj[]>.Dispose(subscribers);
            Events<MarkAllAsRead>.Dispose(subscribers);
            Events<ForumStatus>.Dispose(subscribers);
        }
    }
}
