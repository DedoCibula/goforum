﻿using System;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Helpers;

namespace GoForum.Services
{
    public class UserService : AbstractService, IUserService
    {
        public UserService(string url)
            : base(url)
        {

        }


        public void Login(string loginName, string password, bool anonymous = false, bool push = false)
        {
            if (string.IsNullOrEmpty(loginName))
            {
                Events<Login>.OnError(new ArgumentException("loginName"));
                return;
            }

            if (string.IsNullOrEmpty(password))
            {
                Events<Login>.OnError(new ArgumentException("password"));
                return;
            }    

            //Configuration conf = App.Configurations[Proxy.Url];

            //if (conf.SupportsMd5 == "1")
            //{
            //    password = CryptoHelper.GetMd5Hash(password);
            //}
            //if (conf.SupportsSha1 == "1")
            //{
            //    password = CryptoHelper.GetSha1Hash(password);
            //}

            Monitor.Acquire(TimeSpan.FromSeconds(200));

            Proxy.SetCookies(App.Cookies[Proxy.Url, true]);   
            Proxy.BeginLogin(loginName, password, anonymous, push, result =>
                {
                    try
                    {
                        Events<Login>.OnNext(Proxy.EndLogin(result));
                    }
                    catch (Exception e)
                    {
                        Events<Login>.OnError(e);
                    }
                    finally
                    {
                        Monitor.Release();
                    }
                });
        }


        public void Logout()
        {
            Monitor.Acquire(TimeSpan.FromSeconds(200));

            Proxy.SetCookies(App.Cookies[Proxy.Url]);
            Proxy.BeginLogout(result =>
                {
                    try
                    {
                        Events<Logout>.OnNext(Proxy.EndLogout(result));
                    }
                    catch (Exception e)
                    {
                        Events<Logout>.OnError(e);
                    }
                    finally
                    {
                        Monitor.Release();
                    }
                });
        }

        public override void Dispose(params IDisposable[] subscribers)
        {
            base.Dispose();
            Events<Login>.Dispose(subscribers);
            Events<Logout>.Dispose(subscribers);
        }
    }
}
