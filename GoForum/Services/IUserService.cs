﻿namespace GoForum.Services
{
    public interface IUserService
    {
        void Login(string username, string password, bool anonymous = false, bool push = false);
        void Logout();
    }
}
