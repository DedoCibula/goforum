﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Models.Tapatalk_Structures.InnerStructures;
using GoForum.Services.Tapatalk_API;
using Ninject;

namespace GoForum.Services
{
    public class TopicService : AbstractService, ITopicService
    {
        public TopicService(string url)
            : base(url)
        {
        }

        public void NewTopic(int forumId, string subject, string textBody, int? prefixId, int[] attachementIds, int? groupId)
        {
            if (!App.IsLoggedIn(Proxy.Url))
            {
                Events<NewTopic>.OnError(new Exception("You must be logged to forum in order to create new topic"));
                return;
            }
            if (forumId < 0)
            {
                Events<NewTopic>.OnError(new ArgumentException("forumId cannot be negative"));
                return;
            }
            if (string.IsNullOrEmpty(subject) || string.IsNullOrEmpty(textBody))
            {
                Events<NewTopic>.OnError(new ArgumentNullException(subject == null ? "subject" : "textBody"));
                return;
            }
            if (prefixId.HasValue && attachementIds != null)
            {
                Events<NewTopic>.OnError(new ArgumentException("prefixId should be null if attachementIds is not null"));
                return;
            }
            if (attachementIds != null && groupId.HasValue || groupId < 0)
            {
                Events<NewTopic>.OnError(new ArgumentException("goupId must be set to non-negative value if attachementIds is specified"));
                return;
            }

            Monitor.Acquire(TimeSpan.FromSeconds(200));

            Proxy.SetCookies(App.Cookies[Proxy.Url]);

            Proxy.BeginNewTopic(forumId, subject, textBody, prefixId, attachementIds, groupId, result =>
            {
                try
                {
                    var topic = Proxy.EndNewTopic(result);
                    if (!topic.Result)
                        throw new Exception(topic.ResultText);
                    Events<NewTopic>.OnNext(topic);
                }
                catch (Exception e)
                {
                    Events<NewTopic>.OnError(e);
                }
                finally
                {
                    Monitor.Release();
                }
            });

        }

        public void GetTopic(int forumId, int? startNum, int? lastNum, TopicMode mode)
        {
            if (forumId < 0)
            {
                Events<GetTopic>.OnError(new ArgumentException("forumId cannot be negative"));
                return;
            }
            if ((startNum.HasValue && !lastNum.HasValue) ||
                (!startNum.HasValue && lastNum.HasValue))
            {
                Events<GetTopic>.OnError(new ArgumentException("You must provide both startNum and lastNum parameter"));
                return;
            }
            if (startNum.HasValue && startNum > lastNum)
            {
                Events<GetTopic>.OnError(new ArgumentException("startNum must be inferior to lastNum"));
                return;
            }

            Monitor.Acquire(TimeSpan.FromSeconds(200));

            Proxy.SetCookies(App.Cookies[Proxy.Url]);

            Proxy.BeginGetTopic(forumId, startNum, lastNum, mode.ToString(), result =>
                {
                    try
                    {
                        var topic = Proxy.EndGetTopic(result);
                        if (!topic.Result)
                            throw new Exception(topic.ResultText);
                        Events<GetTopic>.OnNext(topic);
                    }
                    catch (Exception e)
                    {
                        Events<GetTopic>.OnError(e);
                    }
                    finally
                    {
                        Monitor.Release();
                    }
                });
        }

        public void GetSubscribedTopics(int startNum, int lastNum)
        {
            Monitor.Acquire(TimeSpan.FromSeconds(200));

            Proxy.SetCookies(App.Cookies[Proxy.Url]);

            Proxy.BeginGetSubscribedTopics(startNum, lastNum, result =>
            {
                try
                {
                    var subs = Proxy.EndGetSubscribedTopics(result);
                    if (!subs.Result)
                        throw new Exception(subs.ResultText);
                    Events<GetSubscribedTopics>.OnNext(subs);
                }
                catch (Exception e)
                {
                    Events<GetSubscribedTopics>.OnError(e);
                }
                finally
                {
                    Monitor.Release();
                }
            });
        }


        public void SubscribeTopic(int topicID)
        {
            Monitor.Acquire(TimeSpan.FromSeconds(200));

            Proxy.SetCookies(App.Cookies[Proxy.Url]);

            Proxy.BeginSubscribeTopic(topicID, result =>
            {
                try
                {
                    var subs = Proxy.EndSubscribeTopic(result);

                    Events<SubscribeTopic>.OnNext(subs);
                }
                catch (Exception e)
                {
                    Events<SubscribeTopic>.OnError(e);
                }
                finally
                {
                    Monitor.Release();
                }
            });
        }

        public void UnsubscribeTopic(int topicID)
        {
            Monitor.Acquire(TimeSpan.FromSeconds(200));

            Proxy.SetCookies(App.Cookies[Proxy.Url]);

            Proxy.BeginUnsubscribeTopic(topicID, result =>
            {
                try
                {
                    var subs = Proxy.EndUnsubscribeTopic(result);

                    Events<UnsubscribeTopic>.OnNext(subs);
                }
                catch (Exception e)
                {
                    Events<UnsubscribeTopic>.OnError(e);
                }
                finally
                {
                    Monitor.Release();
                }
            });
        }

        public override void Dispose(params IDisposable[] subscribers)
        {
            base.Dispose();
            Events<GetSubscribedTopics>.Dispose(subscribers);
            Events<NewTopic>.Dispose(subscribers);
            Events<GetTopic>.Dispose(subscribers);
            Events<SubscribeTopic>.Dispose(subscribers);
            Events<UnsubscribeTopic>.Dispose(subscribers);
        }
    }
}
