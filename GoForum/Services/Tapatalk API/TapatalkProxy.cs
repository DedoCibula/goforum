﻿using System;
using CookComputing.XmlRpc;
using System.Reflection;
using System.Text;
using System.Linq;
using GoForum.Models.Tapatalk_Structures;
using System.Collections.Generic;
using GoForum.Models.Tapatalk_Structures.InnerStructures;
using Thread = GoForum.Models.Tapatalk_Structures.Thread;

namespace GoForum.Services.Tapatalk_API
{
    public class TapatalkProxy : XmlRpcClientProtocol
    {
        public TapatalkProxy()
        {
            this.Timeout = 5000;
        }

        #region Forum API

        /// <summary>
        /// This function is always the first function to invoke when the app attempts to enter a specific forum. 
        /// It returns configuration name/value pair for this forum system. There are two kind of name/value pairs. 
        /// One is those based on some of the forum system configuration, and one is a simple name/value pairs 
        /// declared in the config.txt file, usually located in mobiquo/conf/config.txt. E.g. the "guest_okay" 
        /// is based on the forum system configuration. while the "api_level" is mobiquo
        /// </summary>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("get_config")]
        public IAsyncResult BeginGetConfiguration(AsyncCallback acb)
        {
            return BeginInvoke(MethodBase.GetCurrentMethod(), new object[0], acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginGetConfiguration
        /// </summary>
        /// <param name="iasr">result obtained from BeginGetConfiguration</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public Configuration EndGetConfiguration(IAsyncResult iasr)
        {
            return EndInvoke<Configuration>(iasr);
        }

        /// <summary>
        /// Return full forum in a nested tree structure. In API Level 3 no parameter required for this function 
        /// and the sub-forum description. For Level 4, forum description is omitted unless "return_description" is set to true
        /// </summary>
        /// <param name="returnDescription">request to return sub-forum description</param>
        /// <param name="forumId">if this parmeter is presented, return only the immediate child of this forum</param>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("get_forum")]
        public IAsyncResult BeginGetForum(bool returnDescription, int? forumId, AsyncCallback acb)
        {
            // neither parameter is required
            var attributes = forumId == null ? new object[] { returnDescription } : new object[] { returnDescription, forumId.ToString() };
            return BeginInvoke(MethodBase.GetCurrentMethod(), attributes, acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginGetForum
        /// </summary>
        /// <param name="iasr">result obtained from BeginGetForum</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public ForumObj[] EndGetForum(IAsyncResult iasr)
        {
            var forums = EndInvoke<ForumObj[]>(iasr);
            var headers = ((XmlRpcAsyncResult)iasr).ResponseHeaders;

            if (headers.AllKeys.Any(t => t == "Set-Cookie"))
            {
                App.Cookies.SaveCookies(headers["Set-Cookie"], Url);
            }

            return forums;
        }

        /// <summary>
        /// Allows mobile client to access password protected sub-forum. It currently support only sub-forum at leaf level. 
        /// If the password is valid, also return the updated cookies so the client will have access to the subsequent get_topic calls
        /// </summary>
        /// <param name="forumId">sub-forum id REQUIRED</param>
        /// <param name="password">sub-forum password REQUIRED</param>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("login_forum")]
        public IAsyncResult BeginLoginForum(int forumId, string password, AsyncCallback acb)
        {
            // IMPORTANT both parameters are required
            return BeginInvoke(MethodBase.GetCurrentMethod(), new object[] { forumId.ToString(), Encoding.UTF8.GetBytes(password) }, acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginLoginForum
        /// </summary>
        /// <param name="iasr">result obtained from BeginGetForum</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public LoginForum EndLoginForum(IAsyncResult iasr)
        {
            return EndInvoke<LoginForum>(iasr);
        }

        /// <summary>
        /// Return board basic statistics data
        /// </summary>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("get_board_stat")]
        public IAsyncResult BeginGetBoardStatistics(AsyncCallback acb)
        {
            return BeginInvoke(MethodBase.GetCurrentMethod(), new object[0], acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginGetBoardStatistics
        /// </summary>
        /// <param name="iasr">result obtained from BeginGetBoardStatistics</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public BoardStats EndGetBoardStatistics(IAsyncResult iasr)
        {
            return EndInvoke<BoardStats>(iasr);
        }

        /// <summary>
        /// Mark all the unread topics as read
        /// </summary>
        /// <param name="forumId">Specify the actual sub-forum to be marked as read, instead of 
        /// marking the entire board as read. This parameters is only used when "mark_forum=1" 
        /// returns in get_config, to indicate the plugin supports marking sub-forum read</param>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("mark_all_as_read")]
        public IAsyncResult BeginMarkAllAsRead(int? forumId, AsyncCallback acb)
        {
            // parameter is not required
            var attributes = forumId == null ? new object[0] : new object[] { forumId.ToString() };
            return BeginInvoke(MethodBase.GetCurrentMethod(), attributes, acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginMarkAllAsRead
        /// </summary>
        /// <param name="iasr">result obtained from BeginMarkAllAsRead</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public MarkAllAsRead EndMarkAllAsRead(IAsyncResult iasr)
        {
            return EndInvoke<MarkAllAsRead>(iasr);
        }

        /// <summary>
        /// Return a list of sub-forum that user has previously participated in, order by 
        /// the latest date of participation
        /// </summary>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("get_participated_forum")]
        public IAsyncResult BeginGetParticipatedForum(AsyncCallback acb)
        {
            return BeginInvoke(MethodBase.GetCurrentMethod(), new object[0], acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginGetParticipatedForum
        /// </summary>
        /// <param name="iasr">result obtained from BeginGetParticipatedForum</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public XmlRpcStruct EndGetParticipatedForum(IAsyncResult iasr)
        {
            return EndInvoke<XmlRpcStruct>(iasr);
        }

        /// <summary>
        /// Given an arrary of forum_id, returns their details and current status
        /// </summary>
        /// <param name="forumIds">A list of forum_id as an array REQUIRED</param>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("get_forum_status")]
        public IAsyncResult BeginGetForumStatus(int[] forumIds, AsyncCallback acb)
        {
            // IMPORTANT parameter is required
            return BeginInvoke(MethodBase.GetCurrentMethod(), new object[] { forumIds }, acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginGetForumStatus
        /// </summary>
        /// <param name="iasr">result obtained from BeginGetForumStatus</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public ForumStatus EndGetForumStatus(IAsyncResult iasr)
        {
            return EndInvoke<ForumStatus>(iasr);
        }

        #endregion

        #region User API

        /// <summary>
        /// Server returns cookies in HTTP header. Client should store the cookies and pass it back to server for all 
        /// subsequence calls to maintain user session. ** DO NOT include HTTP Cookies in the request header **
        /// </summary>
        /// <param name="loginName">login name</param>
        /// <param name="password">the app should send the encrypted password to the server if there is instruction 
        /// received from get_config. Otherwise send the plain-text password. For example most of the vBulletin systems 
        /// requires md5 encryption by default, while SMF systems support SHA-1 encryption</param>
        /// <param name="anonymous">API Level 4 only. Allow user to login anonymously so the user does not appear in the 
        /// Who's Online list. Useful for background login such as pulling unread PM etc</param>
        /// <param name="push">set this parameter as true if get_config return 'push' as '1' and user does not 
        /// disable push on this forum</param>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("login")]
        public IAsyncResult BeginLogin(string loginName, string password, bool anonymous, bool push, AsyncCallback acb)
        {
            var requestLogin = loginName == null ? new byte[0] : Encoding.UTF8.GetBytes(loginName);
            var requestPassword = password == null ? new byte[0] : Encoding.UTF8.GetBytes(password);

            var attributes = push ? new object[] { requestLogin, requestPassword, anonymous, "1" } : new object[] { requestLogin, requestPassword, anonymous };

            return BeginInvoke(MethodBase.GetCurrentMethod(), attributes, acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginLogin
        /// </summary>
        /// <param name="iasr">result obtained from BeginLogin</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public Login EndLogin(IAsyncResult iasr)
        {
            var loginResult = EndInvoke<Login>(iasr);
            var headers = ((XmlRpcAsyncResult)iasr).ResponseHeaders;

            if (headers.AllKeys.Any(t => t == "Set-Cookie"))
                App.Cookies.SaveCookies(headers["Set-Cookie"], Url);

            return loginResult;
        }

        /// <summary>
        /// Logout user, no input and output required
        /// </summary>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("logout_user")]
        public IAsyncResult BeginLogout(AsyncCallback acb)
        {
            return BeginInvoke(MethodBase.GetCurrentMethod(), new object[0], acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginLogout
        /// </summary>
        /// <param name="iasr">result obtained from BeginLogout</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public Logout EndLogout(IAsyncResult iasr)
        {
            EndInvoke(iasr);

            var headers = ((XmlRpcAsyncResult)iasr).ResponseHeaders;

            App.Cookies.InvalidateCookies(Url);

            return new Logout { Url = headers["Set-Cookie"] };
        }

        #endregion

        #region Topic API

        /// <summary>
        /// Post new topic to a particular forum
        /// IMPORTANT first three parameters are required
        /// </summary>
        /// <param name="forumId">forum id REQUIRED</param>
        /// <param name="subject">subject REQUIRED</param>
        /// <param name="textBody">text body REQUIRED</param>
        /// <param name="prefixId">prefix ID that user has selected. List of prefixes are returned from "get_topic" method</param>
        /// <param name="attachementIds">list of attachment ID that come along with this topic creation. If this parameter is 
        /// supplied, the previous parameter (parfix_id) should also be provided with null value</param>
        /// <param name="groupId">required when attachment_id_array is specified</param>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("new_topic")]
        public IAsyncResult BeginNewTopic(int forumId, string subject, string textBody, int? prefixId, int[] attachementIds, int? groupId, AsyncCallback acb)
        {
            // IMPORTANT first three parameters are required
            var arguments = new List<object> { forumId.ToString(), Encoding.UTF8.GetBytes(subject), Encoding.UTF8.GetBytes(textBody) };

            if (prefixId != null && attachementIds == null)
            {
                arguments.Add(prefixId.ToString());
            }
            else if (attachementIds != null && groupId != null)
            {
                arguments.AddRange(new object[] { null, attachementIds, groupId.ToString() });
            }

            return BeginInvoke(MethodBase.GetCurrentMethod(), arguments.ToArray(), acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginNewTopic
        /// </summary>
        /// <param name="iasr">result obtained from BeginNewTopic</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public NewTopic EndNewTopic(IAsyncResult iasr)
        {
            var newTopic = EndInvoke<NewTopic>(iasr);
            var headers = ((XmlRpcAsyncResult)iasr).ResponseHeaders;

            if (headers.AllKeys.Any(t => t == "Set-Cookie"))
                App.Cookies.SaveCookies(headers["Set-Cookie"], Url);

            return newTopic;
        }

        /// <summary>
        /// Returns a list of topics under a specific forum. It can also return sticky topics and announcement, 
        /// given the "mode" parameter is provided
        /// </summary>
        /// <param name="forumId">forum id REQUIRED</param>
        /// <param name="startNum">for pagination. If start_num = 0 & last_num = 9, it returns first 10 topics from 
        /// the forum, sorted by date (last-in-first-out). If both are not presented, return first 20 topics. if start_num = 0 
        /// and last_num = 0, return the first topic only. If last_num - start_num > 50, returns only first 50 topics starting 
        /// from start_num</param>
        /// <param name="lastNum">see above</param>
        /// <param name="mode">if mode = "TOP", returns sticky topics, if mode = "ANN", returns "Announcement" topics only, 
        /// otherwise returns standard topics</param>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("get_topic")]
        public IAsyncResult BeginGetTopic(int forumId, int? startNum, int? lastNum, string mode, AsyncCallback acb)
        {
            // IMPORTANT forumId is required
            var arguments = new List<object> { forumId.ToString() };
            if (startNum != null && lastNum != null)
                arguments.AddRange(new object[] { startNum, lastNum });
            if (mode != "NORMAL")
            {
                arguments.AddRange(new object[] { 0, 50 });
                arguments.Add(mode);
            }

            return BeginInvoke(MethodBase.GetCurrentMethod(), arguments.ToArray(), acb, mode);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginGetTopic
        /// </summary>
        /// <param name="iasr">result obtained from BeginGetTopic</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public GetTopic EndGetTopic(IAsyncResult iasr)
        {
            var getTopic = EndInvoke<GetTopic>(iasr);

            var headers = ((XmlRpcAsyncResult)iasr).ResponseHeaders;

            if (headers.AllKeys.Any(t => t == "Set-Cookie"))
                App.Cookies.SaveCookies(headers["Set-Cookie"], Url);

            if (getTopic.Topics != null && getTopic.Topics.Length > 0)
            {
                if (iasr.AsyncState.ToString() != "NORMAL")
                {
                    for (int i = 0; i < getTopic.Topics.Length; i++)
                    {
                        if (iasr.AsyncState.ToString() == "ANN")
                        {
                            getTopic.Topics[i].IsAnnouncement = true;
                        }
                        else
                        {
                            getTopic.Topics[i].IsSticky = true;
                        }
                    }
                }

                switch (iasr.AsyncState.ToString())
                {
                    case "NORMAL":
                        getTopic.TopicMode = TopicMode.NORMAL;
                        break;

                    case "ANN":
                        getTopic.TopicMode = TopicMode.ANN;
                        break;

                    case "TOP":
                        getTopic.TopicMode = TopicMode.TOP;
                        break;
                }
            }

            return getTopic;
        }

        #endregion

        #region Post API

        /// <summary>
        /// Reply to an existing topic
        /// </summary>
        /// <param name="forumId">forum id REQUIRED</param>
        /// <param name="topicId">topic id REQUIRED</param>
        /// <param name="subject">subject</param>
        /// <param name="textBody">text body REQUIRED</param>
        /// <param name="attachementIds">list of attachment ID that come along with this topic creation. Please check upload</param>
        /// <param name="groupId">required when attachment_id_array is specified</param>
        /// <param name="returnHtml">only support this parameter when get_config return flag no_refresh_on_post as 1</param>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("reply_post")]
        public IAsyncResult BeginReplyPost(int forumId, int topicId, string subject, string textBody, int[] attachementIds, int? groupId, bool? returnHtml, AsyncCallback acb)
        {
            // IMPORTANT forumId, topicId and textBody are required
            var arguments = new List<object> { forumId.ToString(), topicId.ToString() };
            if (subject != null)
                arguments.Add(Encoding.UTF8.GetBytes(subject));
            arguments.Add(Encoding.UTF8.GetBytes(textBody));
            if (attachementIds != null && groupId != null)
                arguments.AddRange(new object[] { attachementIds, groupId.ToString() });
            if (returnHtml != null)
                arguments.Add(returnHtml);

            return BeginInvoke(MethodBase.GetCurrentMethod(), arguments.ToArray(), acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginReplyPost
        /// </summary>
        /// <param name="iasr">result obtained from BeginReplyPost</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public ReplyPost EndReplyPost(IAsyncResult iasr)
        {
            var replyPost = EndInvoke<ReplyPost>(iasr);
            var headers = ((XmlRpcAsyncResult)iasr).ResponseHeaders;

            if (headers.AllKeys.Any(t => t == "Set-Cookie"))
                App.Cookies.SaveCookies(headers["Set-Cookie"], Url);

            return replyPost;
        }

        /// <summary>
        /// Returns a list of posts under the same thread, given a topic_id
        /// </summary>
        /// <param name="topicId">topic ID of the thread</param>
        /// <param name="startNum">For pagination. If start_num = 0 & last_num = 9, it returns first 10 posts 
        /// from the topic. If both are not presented, return first 20 posts. if start_num = 0 and last_num = 0, 
        /// return the first post only, and so on (e.g. 1,1; 2,2). If start_num smaller than last_num returns null. 
        /// If last_num - start_num > 50, returns only first 50 posts starting from start_num REQUIRED</param>
        /// <param name="lastNum">last number REQUIRED</param>
        /// <param name="returnHtml">return html as response</param>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("get_thread")]
        public IAsyncResult BeginGetThread(int? topicId, int startNum, int lastNum, bool? returnHtml, AsyncCallback acb)
        {
            // IMPORTAND startNum and lastNum are required
            var arguments = new List<object>();
            if (topicId != null)
                arguments.Add(topicId.ToString());
            arguments.AddRange(new object[] { startNum, lastNum });
            if (returnHtml != null)
                arguments.Add(returnHtml);
            return BeginInvoke(MethodBase.GetCurrentMethod(), arguments.ToArray(), acb, null);
        }

        /// <summary>
        /// This method is called in callback to get results from BeginGetThread
        /// </summary>
        /// <param name="iasr">result obtained from BeginGetThread</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public Thread EndGetThread(IAsyncResult iasr)
        {
            var thread = EndInvoke<Thread>(iasr);
            var headers = ((XmlRpcAsyncResult)iasr).ResponseHeaders;

            if (headers.AllKeys.Any(t => t == "Set-Cookie"))
                App.Cookies.SaveCookies(headers["Set-Cookie"], Url);

            return thread;
        }

        #endregion

        #region Subscriptiption API
        /// <summary>
        /// Returns a list of topics under a specific forum. It can also return sticky topics and announcement, 
        /// given the "mode" parameter is provided
        /// </summary>
        /// <param name="startNum">for pagination. If start_num = 0 & last_num = 9, it returns first 10 topics from 
        /// the forum, sorted by date (last-in-first-out). If both are not presented, return first 20 topics. if start_num = 0 
        /// and last_num = 0, return the first topic only. If last_num - start_num > 50, returns only first 50 topics starting 
        /// from start_num</param>
        /// <param name="lastNum">see above</param>
        /// <param name="acb">callback after method executes</param>
        /// <returns>asynchronous result to be processed</returns>
        [XmlRpcBegin("get_subscribed_topic")]
        public IAsyncResult BeginGetSubscribedTopics(int? startNum, int? lastNum, AsyncCallback acb)
        {
            var arguments = new List<object>();
            if (startNum != null && lastNum != null)
                arguments.AddRange(new object[] { startNum, lastNum });

            return BeginInvoke(MethodBase.GetCurrentMethod(), arguments.ToArray(), acb, null);
        }


        /// <summary>
        /// This method is called in callback to get results from BeginGetSubscribedTopics
        /// </summary>
        /// <param name="iasr">result obtained from BeginGetSubscribedTopics</param>
        /// <returns>TODO</returns>
        [XmlRpcEnd]
        public GetSubscribedTopics EndGetSubscribedTopics(IAsyncResult iasr)
        {
            var getSubscribedTopics = EndInvoke<GetSubscribedTopics>(iasr);

            var headers = ((XmlRpcAsyncResult)iasr).ResponseHeaders;

            if (headers.AllKeys.Any(t => t == "Set-Cookie"))
                App.Cookies.SaveCookies(headers["Set-Cookie"], Url);

            return getSubscribedTopics;
        }


        [XmlRpcBegin("subscribe_topic")]
        public IAsyncResult BeginSubscribeTopic(int topicID, AsyncCallback acb)
        {
            var arguments = new List<object> { topicID.ToString() };

            return BeginInvoke(MethodBase.GetCurrentMethod(), arguments.ToArray(), acb, null);
        }


        [XmlRpcEnd]
        public SubscribeTopic EndSubscribeTopic(IAsyncResult iasr)
        {
            var getSubscribedTopics = EndInvoke<SubscribeTopic>(iasr);

            var headers = ((XmlRpcAsyncResult)iasr).ResponseHeaders;

            if (headers.AllKeys.Any(t => t == "Set-Cookie"))
                App.Cookies.SaveCookies(headers["Set-Cookie"], Url);

            return getSubscribedTopics;
        }



        [XmlRpcBegin("unsubscribe_topic")]
        public IAsyncResult BeginUnsubscribeTopic(int topicID, AsyncCallback acb)
        {
            var arguments = new List<object> { topicID.ToString() };

            return BeginInvoke(MethodBase.GetCurrentMethod(), arguments.ToArray(), acb, null);
        }


        [XmlRpcEnd]
        public UnsubscribeTopic EndUnsubscribeTopic(IAsyncResult iasr)
        {
            var getSubscribedTopics = EndInvoke<UnsubscribeTopic>(iasr);

            var headers = ((XmlRpcAsyncResult)iasr).ResponseHeaders;

            if (headers.AllKeys.Any(t => t == "Set-Cookie"))
                App.Cookies.SaveCookies(headers["Set-Cookie"], Url);

            return getSubscribedTopics;
        }

        #endregion
    }
}
