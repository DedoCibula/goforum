﻿using System;
using GoForum.Models.Tapatalk_Structures;

namespace GoForum.Services
{
    public class PostService : AbstractService, IPostService
    {
        public PostService(string url)
            : base(url)
        {
        }

        public void ReplyPost(int forumId, int topicId, string subject, string textBody, int[] attachementIds = null, int? groupId = null, bool? returnHtml = null)
        {
            if (forumId <= 0)
            {
                Events<ReplyPost>.OnError(new ArgumentException("forumId"));
                return;
            }
            if (topicId <= 0)
            {
                Events<ReplyPost>.OnError(new ArgumentException("topicId"));
                return;
            }
            if (string.IsNullOrEmpty(subject))
            {
                Events<ReplyPost>.OnError(new ArgumentException("subject"));
                return;
            }
            if (string.IsNullOrEmpty(textBody))
            {
                Events<ReplyPost>.OnError(new ArgumentException("textBody"));
                return;
            }

            Monitor.Acquire(TimeSpan.FromSeconds(200));
           
            Proxy.SetCookies(App.Cookies[Proxy.Url]);
            Proxy.BeginReplyPost(forumId, topicId, subject, textBody, attachementIds, groupId, returnHtml, result =>
                {
                    try
                    {
                        Events<ReplyPost>.OnNext(Proxy.EndReplyPost(result));
                    }
                    catch (Exception e)
                    {
                        Events<ReplyPost>.OnError(e);
                    }
                    finally
                    {
                        Monitor.Release();
                    }
                });
        }


        public void GetThread(int? topicId, int startNum, int lastNum, bool? returnHtml = false)
        {
            if (topicId.HasValue && topicId.Value <= 0)
            {
                Events<Thread>.OnError(new ArgumentException("forumId"));
                return;
            }
            if (startNum < 0)
            {
                Events<Thread>.OnError(new ArgumentException("topicId"));
                return;
            }
            if (lastNum < 0)
            {
                Events<Thread>.OnError(new ArgumentException("topicId"));
                return;
            }

            Monitor.Acquire(TimeSpan.FromSeconds(200));

            Proxy.SetCookies(App.Cookies[Proxy.Url]);

            Proxy.BeginGetThread(topicId, startNum, lastNum, returnHtml, result =>
                {
                    try
                    {
                        Events<Thread>.OnNext(Proxy.EndGetThread(result));
                    }
                    catch (Exception e)
                    {
                        Events<Thread>.OnError(e);
                    }
                    finally
                    {
                        Monitor.Release();
                    }
                });
        }

        public override void Dispose(params IDisposable[] subscribers)
        {
            base.Dispose();
            Events<ReplyPost>.Dispose(subscribers);
            Events<Thread>.Dispose(subscribers);
        }
    }
}
