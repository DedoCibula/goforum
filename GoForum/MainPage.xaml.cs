﻿using System;
using System.Windows;
using System.Windows.Controls;
using GoForum.Models;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Resx;
using GoForum.Services;
using Microsoft.Phone.Controls;
using System.Windows.Media;
using System.Threading.Tasks;
using GoForum.Helpers;

namespace GoForum
{
    public partial class MainPage : PhoneApplicationPage
    {
        private FavoriteForumProvider _favForumProvider;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            Loaded += MainPage_Loaded;

            _favForumProvider = new FavoriteForumProvider();

            //use in code
            var title = AppResources.Title;
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            var i = 0;

            TilesGrid.Children.Clear();

            foreach (var forum in _favForumProvider.GetForums())
            {
                var row = Convert.ToInt32(Math.Floor(i / 2));
                var column = i % 2;

                if (column == 0)
                {
                    TilesGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                }

                var tile = CreateTile(forum);

                tile.SetValue(Grid.RowProperty, row);
                tile.SetValue(Grid.ColumnProperty, column);
                tile.Tap += HubTile_Tap;

                TilesGrid.Children.Add(tile);
                i++;
            }

            // Adjustment to stay in good position and show title
            Task.Run(() => 
            {
                System.Threading.Thread.Sleep(300);

                Dispatcher.BeginInvoke(() =>
                {
                    foreach (var control in TilesGrid.Children)
                    {
                        if (control is HubTile)
                        {
                            HubTile tt = ((HubTile)control);

                            VisualStateManager.GoToState(tt, "Semiexpanded", false);
                            tt.IsFrozen = true;
                        }
                    }
                });
            });
        }

        private HubTile CreateTile(FavoriteForum forum)
        {
            var tile = new HubTile
            {
                Name = forum.Name,
                Title = forum.Name,
                Message = forum.Name,
                Tag = forum,
                Size = TileSize.Medium,
                Margin = new Thickness(10, 10, 10, 10)
            };

            var contextMenu = new ContextMenu();
            var menuItem = new MenuItem {Header = "edit"};
            menuItem.Click += EditAction_OnClick;
            contextMenu.Items.Add(menuItem);
            menuItem.DataContext = forum;
            ContextMenuService.SetContextMenu(tile, contextMenu);

            return tile;
        }

        IDisposable loginSub;
        IDisposable confSub;

        private void HubTile_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var tile = sender as HubTile;

            var forum = (FavoriteForum)tile.Tag;
            App.CurrentForumUrl = forum.MobiquoUrl;

            var forumService = IoCContainer.Get<IForumService>(App.CurrentForumUrl);
            var userService = IoCContainer.Get<IUserService>(App.CurrentForumUrl);

            confSub = Events<Configuration>.Subscribe(conf =>
                {
                    if (conf.Result)
                    {
                        loginSub = Events<Login>.Subscribe(login =>
                            {
                                if (login.Result)
                                {
                                    StateHelper.SetToCurrentState(tile.Title, States.CATEGORIES_TITLE);
                                    NavigationService.Navigate(new Uri("/Views/ForumOverviewView.xaml", UriKind.Relative));
                                }
                                else
                                {
                                    MessageBox.Show(login.ResultText);
                                }

                                EndLoging(tile);
                            },
                            error =>
                            {
                                MessageBox.Show(error.Message);

                                EndLoging(tile);
                            });

                        userService.Login(forum.Username, forum.Password);
                    }
                    else
                    {
                        MessageBox.Show(conf.ResultText);

                        EndLoging(tile);
                    }
                },
                error =>
                {
                    MessageBox.Show(error.Message);

                    EndLoging(tile);
                });

            StartLoging(tile);
            Task.Run(() => forumService.GetConfiguration());
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/AddForum.xaml", UriKind.RelativeOrAbsolute));
        }

        Brush brush = null;

        private void StartLoging(HubTile tile)
        {
            foreach (var control in TilesGrid.Children)
            {
                if (control is HubTile)
                {
                    ((HubTile)control).IsEnabled = false;
                }
            }

            this.ShowProgressIndicator("Trying login into the forum");

            tile.IsEnabled = false;
            brush = tile.Background;
            tile.Background = new SolidColorBrush(Colors.Gray);
        }

        private void EndLoging(HubTile tile)
        {
            foreach (var control in TilesGrid.Children)
            {
                if (control is HubTile)
                {
                    ((HubTile)control).IsEnabled = true;
                }
            }

            this.HideProgressIndicator();

            tile.IsEnabled = true;
            tile.Background = brush;

            Events<Login>.Dispose(loginSub);
            Events<Configuration>.Dispose(confSub);
        }

        private void EditAction_OnClick(object sender, RoutedEventArgs e)
        {
            var forum = ((MenuItem) sender).DataContext as FavoriteForum;
            EditForum(forum);
        }

        private void EditForum(FavoriteForum forum)
        {
            NavigationService.Navigate(new Uri("/Views/EditForum.xaml?mobUrl=" + forum.MobiquoUrl, UriKind.RelativeOrAbsolute));
        }

        private void ApplicationBarMenuItem_OnClick(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/SettingsPage.xaml", UriKind.RelativeOrAbsolute));
        }

    }
}