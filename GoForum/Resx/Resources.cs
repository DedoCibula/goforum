﻿using GoForum.Resx;

namespace GoForum
{
    public class Resources
    {
        private static readonly AppResources _appResources = new AppResources();

        public AppResources AppResources
        {
            get { return _appResources; }
        }
    }
}
