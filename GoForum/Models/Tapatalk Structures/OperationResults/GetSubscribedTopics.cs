﻿using CookComputing.XmlRpc;
using GoForum.Models.Tapatalk_Structures.InnerStructures;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class GetSubscribedTopics : OperationResult
    {
        /// <summary>
        /// Total number of topics in this forum. 
        /// If this forum has no topic, it returns 0, and the "topics" key returns return null
        /// </summary>
        [XmlRpcMember("total_topic_num")]
        public int TopicsCount { get; set; }


        [XmlRpcMember("forum_id")]
        public string ForumID { get; set; }

        /// <summary>
        /// Forum name.
        /// </summary>
        [XmlRpcMember("forum_name")]
        public byte[] ForumNameByte { get; set; }

        /// <summary>
        /// Forum name.
        /// </summary>

        public string ForumName
        {
            get { return ForumNameByte.GetString(); }
        }

        /// <summary>
        /// Returns a list of topic in an array, each topic is in Hash Table format. Returns null if total_topic_num = 0
        /// </summary>

        [XmlRpcMember("topics")]
        public Topic[] Topics { get; set; }


        public TopicMode TopicMode { get; set; }
    }
}
