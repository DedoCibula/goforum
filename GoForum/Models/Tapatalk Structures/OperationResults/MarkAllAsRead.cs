﻿using CookComputing.XmlRpc;
namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class MarkAllAsRead : OperationResult
    {
    }
}
