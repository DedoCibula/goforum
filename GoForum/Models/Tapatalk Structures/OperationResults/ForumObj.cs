﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class ForumObj : ForumBase
    {
        /// <summary>
        /// Parent's forum ID of this forum, returns -1 if this forum is the root forum.
        /// </summary>
        [XmlRpcMember("parent_id")]
        public string ParentId { get; set; }


        /// <summary>
        /// Description of the forum. 
        /// If required_description is set to true.
        /// </summary>

        [XmlRpcMember("description")]
        public byte[] DescriptionByte { get; set; }


        /// <summary>
        /// Description of the forum. 
        /// If required_description is set to true.
        /// </summary>

        public string Description
        {
            get { return DescriptionByte.GetString(); }
        }


        /// <summary>
        /// Returns true if the user has previously subscribed to this forum.
        /// </summary>

        [XmlRpcMember("is_subscribed")]
        public bool IsSubscribed { get; set; }


        /// <summary>
        /// Returns false if the subscription feature is turned off.
        /// </summary>

        [XmlRpcMember("can_subscribe")]
        public bool SubscriptionAvailable { get; set; }


        /// <summary>
        /// If it contains a url, it means this forum is just a link to other webpage.
        /// </summary>

        [XmlRpcMember("url")]
        public string Url { get; set; }


        /// <summary>
        /// Identify whether this forum is only a placeholder ("Category" in vBulletin term) for sub-forums (sub_only = true), or can contains both topics and forums (sub_only = false).
        /// </summary>

        [XmlRpcMember("sub_only")]
        public bool IsSubForumOnly { get; set; }


        /// <summary>
        /// If this forum is not a "leaf" forum, returns a list of child forum in an array of hash. 
        /// This "child" key should return an array of hash. This entire structure can be nested. 
        /// Client assume this is a leaf forum if this key is missing from the response.
        /// </summary>

        [XmlRpcMember("child")]
        public ForumObj[] ChildForums { get; set; }
    }
}

/*

<?xml version="1.0" encoding="UTF-8"?>
<methodResponse>
<params>
<param>
<value>
<array>
<data>
<value><struct>
<member><name>forum_id</name>
<value><string>1</string></value>
</member>
<member><name>forum_name</name>
<value><base64>WW91ciBmaXJzdCBjYXRlZ29yeQ==</base64></value>
</member>
<member><name>parent_id</name>
<value><string>0</string></value>
</member>
<member><name>logo_url</name>
<value><string></string></value>
</member>
<member><name>url</name>
<value><string></string></value>
</member>
<member><name>can_subscribe</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_subscribed</name>
<value><boolean>0</boolean></value>
</member>
<member><name>sub_only</name>
<value><boolean>1</boolean></value>
</member>
<member><name>child</name>
<value><array>
<data>
<value><struct>
<member><name>forum_id</name>
<value><string>2</string></value>
</member>
<member><name>forum_name</name>
<value><base64>WW91ciBmaXJzdCBmb3J1bQ==</base64></value>
</member>
<member><name>parent_id</name>
<value><string>1</string></value>
</member>
<member><name>logo_url</name>
<value><string></string></value>
</member>
<member><name>url</name>
<value><string></string></value>
</member>
<member><name>can_subscribe</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_subscribed</name>
<value><boolean>0</boolean></value>
</member>
</struct></value>
</data>
</array></value>
</member>
</struct></value>
</data>
</array></value>
</param>
</params>
</methodResponse>
*/