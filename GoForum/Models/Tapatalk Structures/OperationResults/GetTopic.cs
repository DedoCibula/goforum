﻿using CookComputing.XmlRpc;
using GoForum.Models.Tapatalk_Structures.InnerStructures;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class GetTopic : OperationResult
    {
        /// <summary>
        /// Total number of topics in this forum. 
        /// If this forum has no topic, it returns 0, and the "topics" key returns return null
        /// </summary>
        [XmlRpcMember("total_topic_num")]
        public int TopicsCount { get; set; }


        [XmlRpcMember("forum_id")]
        public string ForumID { get; set; }


        /// <summary>
        /// Forum name.
        /// </summary>
        [XmlRpcMember("forum_name")]
        public byte[] ForumNameByte { get; set; }


        /// <summary>
        /// Forum name.
        /// </summary>

        public string ForumName
        {
            get { return ForumNameByte.GetString(); }
        }


        /// <summary>
        /// return false if user cannot create new topic in this forum
        /// </summary>

        [XmlRpcMember("can_post")]
        public bool CanPost { get; set; }


        /// <summary>
        /// Add a "unread_sticky_count" and "unread_announce_count" integer value in get_topic first level Hash-table to indicate users there are unread stickies topics and announcement in this sub-forum. 
        /// This allow the app to show a unread badge when entering a sub-forum.
        /// </summary>

        [XmlRpcMember("unread_sticky_count")]
        public int UnreadStickyCount { get; set; }



        [XmlRpcMember("unread_announce_count")]
        public int UnreadAnnounceCount { get; set; }


        /// <summary>
        /// return true if current user can subscribe this forum. Default as true for member.
        /// </summary>

        [XmlRpcMember("can_subscribe")]
        public bool CanSubscribe { get; set; }


        /// <summary>
        /// This is used in conjunction with existing "prefixes" in get_topic function, to indicate whether users need to select a prefix mandatory. 
        /// If this field is missing, assuming mandatory.
        /// </summary>

        [XmlRpcMember("require_prefix")]
        public bool RequirePrefix { get; set; }


        /// <summary>
        /// if this is not empty, it contains a list of prefixes, it indicates user need to enter prefix in order to create a new topic successfully
        /// </summary>

        [XmlRpcMember("prefixes")]
        public Prefix[] Prefixes { get; set; }


        /// <summary>
        /// Returns a list of topic in an array, each topic is in Hash Table format. Returns null if total_topic_num = 0
        /// </summary>

        [XmlRpcMember("topics")]
        public Topic[] Topics { get; set; }



        public TopicMode TopicMode { get; set; }
    }
}
