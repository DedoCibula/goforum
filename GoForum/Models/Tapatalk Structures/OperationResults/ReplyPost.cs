﻿using System;
using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class ReplyPost : OperationResult
    {
        /// <summary>
        /// the newly generated post ID for this new topic.
        /// </summary>
        [XmlRpcMember("post_id")]
        public string PostID { get; set; }


        /// <summary>
        /// 1 = post is success but need moderation. 
        /// Otherwise no need to return this key. 
        /// If this is return. 
        /// The app should not refresh the thread view as the post will not be appear, instead it should display an alert to user about this post is pending for moderation.
        /// </summary>

        [XmlRpcMember("state")]
        public int State { get; set; }


        /// <summary>
        /// Return the newly replied post content and all below keys back to the App if get_config return support no_refresh_on_post. 
        /// Characters display rules (follow the sequence): 1) Remove all BBCode except [ur], [img], [quote]. 2) 
        /// Remove all non-displayable characters (e.g. \n, \t, white-space, etc) at the beginning AND the end of the content (Trimming) 3) Replace [url] tags in nested [url][img][/img][/url] image link.
        /// </summary>

        [XmlRpcMember("post_content")]
        public byte[] PostContentByte { get; set; }


        /// <summary>
        /// Return the newly replied post content and all below keys back to the App if get_config return support no_refresh_on_post. 
        /// Characters display rules (follow the sequence): 1) Remove all BBCode except [ur], [img], [quote]. 2) 
        /// Remove all non-displayable characters (e.g. \n, \t, white-space, etc) at the beginning AND the end of the content (Trimming) 3) Replace [url] tags in nested [url][img][/img][/url] image link.
        /// </summary>

        public string PostContent
        {
            get { return PostContentByte.GetString(); }
        }


        /// <summary>
        /// return true if user can edit this post.
        /// </summary>

        [XmlRpcMember("can_edit")]
        public bool CanEdit { get; set; }


        /// <summary>
        /// return true if user can delete this post.
        /// </summary>

        [XmlRpcMember("can_delete")]
        public bool CanDelete { get; set; }


        /// <summary>
        /// dateTime.iso8601 format. If this topic has no reply, use the topic creation time.
        /// </summary>

        [XmlRpcMember("post_time")]
        public DateTime PostTime { get; set; }


        /// <summary>
        /// Returns a list of attachments user has uploaded within this post, in array of hash format.
        /// </summary>

        [XmlRpcMember("attachements")]
        public Attachement[] Attachements { get; set; }
    }
}
