﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class NewTopic : OperationResult
    {
        /// <summary>
        /// The newly generated topic ID for this new topic.
        /// </summary>

        [XmlRpcMember("topic_id")]
        public string TopicID { get; set; }


        /// <summary>
        /// 1 = post is success but need moderation. Otherwise no need to return this key.
        /// </summary>

        [XmlRpcMember("state")]
        public int State { get; set; }
    }
}
