﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class Thread : OperationResult
    {
        /// <summary>
        /// total number of posts in this topic
        /// </summary>
        [XmlRpcMember("total_post_num")]
        public int TotalPosts { get; set; }


        [XmlRpcMember("forum_id")]
        public string ForumID { get; set; }


        [XmlRpcMember("forum_name")]

        public byte[] ForumNameByte { get; set; }



        public string ForumName
        {
            get { return ForumNameByte.GetString(); }
        }


        [XmlRpcMember("topic_id")]
        public string TopicID { get; set; }


        [XmlRpcMember("topic_title")]
        public byte[] TopicTitleByte { get; set; }



        public string TopicTitle
        {
            get { return TopicTitleByte.GetString(); }
        }


        [XmlRpcMember("prefix")]
        public byte[] PrefixByte { get; set; }



        public string Prefix
        {
            get { return PrefixByte.GetString(); }
        }


        /// <summary>
        /// return true if this thread has been subscribed by this user.
        /// </summary>

        [XmlRpcMember("is_subscribed")]
        public bool IsSubsribed { get; set; }


        /// <summary>
        /// returns false if the subscription feature is turned off
        /// </summary>

        [XmlRpcMember("can_subscribe")]
        public bool CanSubscribe { get; set; }


        /// <summary>
        /// return true if this thread has been closed. 
        /// If this thread is closed, it should not allow reply or edit unless can_reply (thread) or can_edit (post) are set to "true" explicitly
        /// </summary>

        [XmlRpcMember("is_closed")]
        public bool IsClosed { get; set; }


        /// <summary>
        /// return false if user cannot reply to this thread.
        /// </summary>

        [XmlRpcMember("can_reply")]
        public bool CanReply { get; set; }


        /// <summary>
        /// return false if user cannot reply to this thread.
        /// </summary>
        [XmlRpcMember("posts")]
        public Post[] Posts { get; set; }


        /// <summary>
        /// Returns a list of attachments user has uploaded within this post, in array of hash format.
        /// </summary>

        [XmlRpcMember("attachements")]
        public Attachement[] Attachements { get; set; }


        /// <summary>
        /// Return post thanks user list infor only when thank_post is supported.
        /// </summary>

        [XmlRpcMember("thanks_info")]
        public Thank[] Thanks { get; set; }


        /// <summary>
        /// Return post likes user list infor only when like_post/unlike_post are supported.
        /// </summary>

        [XmlRpcMember("likes_info")]
        public Like[] Likes { get; set; }


        /// <summary>
        /// Forum nav to current thread, order from top forum to it's parent forum
        /// </summary>

        [XmlRpcMember("breadcrumb")]
        public Breadcrumb[] Breadcrumb { get; set; }
    }
}

/*

<?xml version="1.0" encoding="UTF-8"?>
<methodResponse>
<params>
<param>
<value><struct>
<member><name>total_post_num</name>
<value><int>12</int></value>
</member>
<member><name>forum_id</name>
<value><string>2</string></value>
</member>
<member><name>forum_name</name>
<value><base64>WW91ciBmaXJzdCBmb3J1bQ==</base64></value>
</member>
<member><name>topic_id</name>
<value><string>9</string></value>
</member>
<member><name>topic_title</name>
<value><base64>VG9waWMgdGl0bGU=</base64></value>
</member>
<member><name>position</name>
<value><int>1</int></value>
</member>
<member><name>can_reply</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_upload</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_delete</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_move</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_subscribe</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_rename</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_subscribed</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_stick</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_sticky</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_close</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_closed</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_approve</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_approved</name>
<value><boolean>1</boolean></value>
</member>
<member><name>max_attachment</name>
<value><int>0</int></value>
</member>
<member><name>max_png_size</name>
<value><int>0</int></value>
</member>
<member><name>max_jpg_size</name>
<value><int>0</int></value>
</member>
<member><name>posts</name>
<value><array>
<data>
<value><struct>
<member><name>topic_id</name>
<value><string>9</string></value>
</member>
<member><name>post_id</name>
<value><string>11</string></value>
</member>
<member><name>post_title</name>
<value><base64>VG9waWMgdGl0bGU=</base64></value>
</member>
<member><name>post_content</name>
<value><base64>VG9waWMgdGV4dA==</base64></value>
</member>
<member><name>post_author_id</name>
<value><string>2</string></value>
</member>
<member><name>post_author_name</name>
<value><base64>YWRtaW4=</base64></value>
</member>
<member><name>icon_url</name>
<value><string></string></value>
</member>
<member><name>post_time</name>
<value><dateTime.iso8601>20130410T21:32:01+00:00</dateTime.iso8601></value>
</member>
<member><name>timestamp</name>
<value><string>1365629521</string></value>
</member>
<member><name>attachments</name>
<value><array>
<data>
</data>
</array></value>
</member>
<member><name>is_online</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_edit</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_delete</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_approve</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_approved</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_move</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_ban</name>
<value><boolean>0</boolean></value>
</member>
<member><name>allow_smilies</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
<value><struct>
<member><name>topic_id</name>
<value><string>9</string></value>
</member>
<member><name>post_id</name>
<value><string>12</string></value>
</member>
<member><name>post_title</name>
<value><base64>UmU6IERhZnVx</base64></value>
</member>
<member><name>post_content</name>
<value><base64>VGhpcyBpcyBTcGFydGE=</base64></value>
</member>
<member><name>post_author_id</name>
<value><string>2</string></value>
</member>
<member><name>post_author_name</name>
<value><base64>YWRtaW4=</base64></value>
</member>
<member><name>icon_url</name>
<value><string></string></value>
</member>
<member><name>post_time</name>
<value><dateTime.iso8601>20130410T21:52:16+00:00</dateTime.iso8601></value>
</member>
<member><name>timestamp</name>
<value><string>1365630736</string></value>
</member>
<member><name>attachments</name>
<value><array>
<data>
</data>
</array></value>
</member>
<member><name>is_online</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_edit</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_delete</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_approve</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_approved</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_move</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_ban</name>
<value><boolean>0</boolean></value>
</member>
<member><name>allow_smilies</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
<value><struct>
<member><name>topic_id</name>
<value><string>9</string></value>
</member>
<member><name>post_id</name>
<value><string>13</string></value>
</member>
<member><name>post_title</name>
<value><base64>UmU6IERhZnVx</base64></value>
</member>
<member><name>post_content</name>
<value><base64>VGhpcyBpcyBTcGFydGE=</base64></value>
</member>
<member><name>post_author_id</name>
<value><string>2</string></value>
</member>
<member><name>post_author_name</name>
<value><base64>YWRtaW4=</base64></value>
</member>
<member><name>icon_url</name>
<value><string></string></value>
</member>
<member><name>post_time</name>
<value><dateTime.iso8601>20130411T21:58:42+00:00</dateTime.iso8601></value>
</member>
<member><name>timestamp</name>
<value><string>1365717522</string></value>
</member>
<member><name>attachments</name>
<value><array>
<data>
</data>
</array></value>
</member>
<member><name>is_online</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_edit</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_delete</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_approve</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_approved</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_move</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_ban</name>
<value><boolean>0</boolean></value>
</member>
<member><name>allow_smilies</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
<value><struct>
<member><name>topic_id</name>
<value><string>9</string></value>
</member>
<member><name>post_id</name>
<value><string>14</string></value>
</member>
<member><name>post_title</name>
<value><base64>UmU6IERhZnVx</base64></value>
</member>
<member><name>post_content</name>
<value><base64>VGhpcyBpcyBTcGFydGE=</base64></value>
</member>
<member><name>post_author_id</name>
<value><string>2</string></value>
</member>
<member><name>post_author_name</name>
<value><base64>YWRtaW4=</base64></value>
</member>
<member><name>icon_url</name>
<value><string></string></value>
</member>
<member><name>post_time</name>
<value><dateTime.iso8601>20130411T22:00:44+00:00</dateTime.iso8601></value>
</member>
<member><name>timestamp</name>
<value><string>1365717644</string></value>
</member>
<member><name>attachments</name>
<value><array>
<data>
</data>
</array></value>
</member>
<member><name>is_online</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_edit</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_delete</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_approve</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_approved</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_move</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_ban</name>
<value><boolean>0</boolean></value>
</member>
<member><name>allow_smilies</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
<value><struct>
<member><name>topic_id</name>
<value><string>9</string></value>
</member>
<member><name>post_id</name>
<value><string>15</string></value>
</member>
<member><name>post_title</name>
<value><base64>UmU6IERhZnVx</base64></value>
</member>
<member><name>post_content</name>
<value><base64>VGhpcyBpcyBTcGFydGE=</base64></value>
</member>
<member><name>post_author_id</name>
<value><string>2</string></value>
</member>
<member><name>post_author_name</name>
<value><base64>YWRtaW4=</base64></value>
</member>
<member><name>icon_url</name>
<value><string></string></value>
</member>
<member><name>post_time</name>
<value><dateTime.iso8601>20130411T22:03:17+00:00</dateTime.iso8601></value>
</member>
<member><name>timestamp</name>
<value><string>1365717797</string></value>
</member>
<member><name>attachments</name>
<value><array>
<data>
</data>
</array></value>
</member>
<member><name>is_online</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_edit</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_delete</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_approve</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_approved</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_move</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_ban</name>
<value><boolean>0</boolean></value>
</member>
<member><name>allow_smilies</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
<value><struct>
<member><name>topic_id</name>
<value><string>9</string></value>
</member>
<member><name>post_id</name>
<value><string>16</string></value>
</member>
<member><name>post_title</name>
<value><base64>UmU6IERhZnVx</base64></value>
</member>
<member><name>post_content</name>
<value><base64>VGhpcyBpcyBTcGFydGE=</base64></value>
</member>
<member><name>post_author_id</name>
<value><string>2</string></value>
</member>
<member><name>post_author_name</name>
<value><base64>YWRtaW4=</base64></value>
</member>
<member><name>icon_url</name>
<value><string></string></value>
</member>
<member><name>post_time</name>
<value><dateTime.iso8601>20130411T22:04:34+00:00</dateTime.iso8601></value>
</member>
<member><name>timestamp</name>
<value><string>1365717874</string></value>
</member>
<member><name>attachments</name>
<value><array>
<data>
</data>
</array></value>
</member>
<member><name>is_online</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_edit</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_delete</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_approve</name>
<value><boolean>0</boolean></value>
</member>
<member><name>is_approved</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_move</name>
<value><boolean>0</boolean></value>
</member>
<member><name>can_ban</name>
<value><boolean>0</boolean></value>
</member>
<member><name>allow_smilies</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
</data>
</array></value>
</member>
</struct></value>
</param>
</params>
</methodResponse>
*/