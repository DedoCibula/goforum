﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class OperationResult
    {
        private bool mResult = true;

        [XmlRpcMember("result")]
        public bool Result
        {
            get { return mResult; }
            set { mResult = value; }
        }


        [XmlRpcMember("result_text")]
        public byte[] ResultTextByte { get; set; }


        public string ResultText
        {
            get { return ResultTextByte.GetString(); }
        }
    }
}
