﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class ForumBase : OperationResult
    {
        /// <summary>
        /// Forum ID.
        /// </summary>
        [XmlRpcMember("forum_id")]
        public string ForumId { get; set; }


        /// <summary>
        /// Forum name.
        /// </summary>

        [XmlRpcMember("forum_name")]
        public byte[] ForumNameByte { get; set; }


        /// <summary>
        /// Forum name.
        /// </summary>

        public string ForumName
        {
            get { return ForumNameByte.GetString(); }
        }


        /// <summary>
        /// Returns the forum logo art-work URL (absolute URL). 
        /// If any.
        /// </summary>

        [XmlRpcMember("logo_url")]
        public string Logo { get; set; }


        /// <summary>
        /// Returns true if this forum is password protected. 
        /// The app side will call "login_forum" to get authorization before attempting to call get_topic the this forum.
        /// </summary>

        [XmlRpcMember("is_protected")]
        public bool IsProtected { get; set; }


        /// <summary>
        /// Returns true if this forum contains unread topic
        /// </summary>

        [XmlRpcMember("new_post")]
        public bool HasNewPosts { get; set; }
    }
}
