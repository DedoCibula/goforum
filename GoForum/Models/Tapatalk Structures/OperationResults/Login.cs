﻿using CookComputing.XmlRpc;
using System.Net;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class Login : OperationResult
    {
        /// <summary>
        /// Return this user ID.
        /// </summary>
        [XmlRpcMember("user_id")]
        public string UserId { get; set; }


        /// <summary>
        /// Return user's display name if the forum system support display name feature.
        /// </summary>
        [XmlRpcMember("username")]
        public byte[] UsernameByte { get; set; }


        /// <summary>
        /// Return user's display name if the forum system support display name feature.
        /// </summary>

        public string Username
        {
            get { return UsernameByte.GetString(); }
        }


        /// <summary>
        /// Return a list of usergroup ID that this user belongs to.
        /// </summary>
        [XmlRpcMember("usergroup_id")]
        public string[] UserGroups { get; set; }


        /// <summary>
        /// Icon url.
        /// </summary>

        [XmlRpcMember("icon_url")]
        public string Icon { get; set; }


        /// <summary>
        /// User type.
        /// </summary>

        [XmlRpcMember("user_type")]
        public byte[] UserTypeByte { get; set; }


        /// <summary>
        /// User type.
        /// </summary>

        public string UserType
        {
            get { return UserTypeByte.GetString(); }
        }


        /// <summary>
        /// User register email address.
        /// </summary>

        [XmlRpcMember("email")]
        public byte[] EmailByte { get; set; }


        /// <summary>
        /// User register email address.
        /// </summary>

        public string Email
        {
            get { return EmailByte.GetString(); }
        }


        /// <summary>
        /// Return total number of post of this user.
        /// </summary>

        [XmlRpcMember("post_count")]
        public int PostCount { get; set; }


        /// <summary>
        /// This instructs the app to hide the "Messaging" tab so user will not be able to use the PM features in the app.
        /// </summary>

        [XmlRpcMember("can_pm")]
        public bool CanPM { get; set; }


        /// <summary>
        /// This instructs the app to disable "Send PM" feature, however it still allows user to read Private Message, just that the user cannot send private message.
        /// </summary>

        [XmlRpcMember("can_send_pm")]
        public bool CanSendPM { get; set; }


        /// <summary>
        /// Return true if this particular user has moderation capability. Moderation is an extension and not part of the Tapatalk API Level 3.
        /// </summary>

        [XmlRpcMember("can_moderate")]
        public bool CanModerate { get; set; }


        /// <summary>
        /// Return false if users do not have permission to search in this forum.
        /// </summary>

        [XmlRpcMember("can_search")]
        public bool CanSearch { get; set; }


        /// <summary>
        /// Return false if this user does not have permission to see current list of online user.
        /// </summary>

        [XmlRpcMember("can_whosonline")]
        public bool CanSeeWhoIsOnline { get; set; }


        /// <summary>
        /// Return false if this user does not have permission to user profile page (get_user_info).
        /// </summary>

        [XmlRpcMember("can_profile")]
        public bool CanProfile { get; set; }


        /// <summary>
        /// Return true if this user can modify his avatar.
        /// </summary>

        [XmlRpcMember("can_upload_avatar")]
        public bool CanUploadAvatar { get; set; }


        /// <summary>
        /// Return the maximum allowed attachments the user can be uploaded in a single post. 
        /// If max_attachment is missing, it assumes the plugin does not support attachment upload. 
        /// If a user is restricted from uploading attachment, it should use the "can_upload" flag in get_topic and get_thread to determine whether user can upload attachment, regardless how many attachments user can upload within a single post.
        /// </summary>

        [XmlRpcMember("max_attachment")]
        public int MaxAttachmentsInSinglePost { get; set; }


        /// <summary>
        /// Return the maximum allowed PNG file size that the user can upload. This field is used in conjunction with the Attachment section. This number should be in byte.
        /// </summary>

        [XmlRpcMember("max_png_size")]
        public int MaxPngSize { get; set; }


        /// <summary>
        /// Return the maximum allowed JPEG file size that the user can upload. This field is used in conjunction with the Attachment section. This number should be in byte.
        /// </summary>

        [XmlRpcMember("max_jpg_size")]
        public int MaxJpgSize { get; set; }


        /// <summary>
        /// Push types.
        /// </summary>

        [XmlRpcMember("push_type")]
        public PushType[] PushTypes { get; set; }
    }
}

/*

HTTP/1.1 200 OK
Server: nginx/1.2.3
Date: Sat, 06 Apr 2013 18:26:10 GMT
Content-Type: text/xml { get; set; } charset=UTF-8
Content-Length: 2510
Connection: keep-alive
X-Powered-By: PHP/5.4.0-3
Mobiquo_is_login: false
Set-Cookie: mobiquo_a=0
Set-Cookie: mobiquo_b=0
Set-Cookie: mobiquo_c=0
Set-Cookie: phpbb3_ldmhi_u=2 { get; set; } expires=Sun, 06-Apr-2014 18:26:10 GMT { get; set; } path=/ { get; set; } domain=pv239.pavelkouril.cz { get; set; } HttpOnly
Set-Cookie: phpbb3_ldmhi_k=801120d2ffdb51f9 { get; set; } expires=Sun, 06-Apr-2014 18:26:10 GMT { get; set; } path=/ { get; set; } domain=pv239.pavelkouril.cz { get; set; } HttpOnly
Set-Cookie: phpbb3_ldmhi_sid=d2600edf53026a8d63d3f23e828f0bc4 { get; set; } expires=Sun, 06-Apr-2014 18:26:10 GMT { get; set; } path=/ { get; set; } domain=pv239.pavelkouril.cz { get; set; } HttpOnly
Vary: Accept-Charset

<?xml version="1.0" encoding="UTF-8"?>
<methodResponse>
<params>
<param>
<value><struct>
<member><name>result</name>
<value><boolean>1</boolean></value>
</member>
<member><name>user_id</name>
<value><string>2</string></value>
</member>
<member><name>username</name>
<value><base64>YWRtaW4=</base64></value>
</member>
<member><name>email</name>
<value><base64>ZW1haWxAZXhhbXBsZS5jb20=</base64></value>
</member>
<member><name>user_type</name>
<value><base64>YWRtaW4=</base64></value>
</member>
<member><name>usergroup_id</name>
<value><array>
<data>
<value><string>5</string></value>
</data>
</array></value>
</member>
<member><name>icon_url</name>
<value><string></string></value>
</member>
<member><name>post_count</name>
<value><int>4</int></value>
</member>
<member><name>can_pm</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_send_pm</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_moderate</name>
<value><boolean>1</boolean></value>
</member>
<member><name>max_attachment</name>
<value><int>3</int></value>
</member>
<member><name>max_png_size</name>
<value><int>262144</int></value>
</member>
<member><name>max_jpg_size</name>
<value><int>262144</int></value>
</member>
<member><name>can_search</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_whosonline</name>
<value><boolean>1</boolean></value>
</member>
<member><name>can_upload_avatar</name>
<value><boolean>1</boolean></value>
</member>
<member><name>push_type</name>
<value><array>
<data>
<value><struct>
<member><name>name</name>
<value><string>pm</string></value>
</member>
<member><name>value</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
<value><struct>
<member><name>name</name>
<value><string>sub</string></value>
</member>
<member><name>value</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
<value><struct>
<member><name>name</name>
<value><string>quote</string></value>
</member>
<member><name>value</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
<value><struct>
<member><name>name</name>
<value><string>newtopic</string></value>
</member>
<member><name>value</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
<value><struct>
<member><name>name</name>
<value><string>tag</string></value>
</member>
<member><name>value</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
</data>
</array></value>
</member>
</struct></value>
</param>
</params>
</methodResponse>
*/