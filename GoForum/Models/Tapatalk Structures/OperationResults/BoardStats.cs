﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class BoardStats : OperationResult
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlRpcMember("total_threads")]
        public int TotalThreads { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [XmlRpcMember("total_posts")]
        public int TotalPosts { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [XmlRpcMember("total_members")]
        public int TotalMembers { get; set; }



        /// <summary>
        /// 
        /// </summary>

        [XmlRpcMember("active_members")]
        public int ActiveMembers { get; set; }


        /// <summary>
        /// total online visitors including guests
        /// </summary>
        [XmlRpcMember("total_online")]
        public int TotalOnline { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [XmlRpcMember("guest_online")]
        public int GuestOnline { get; set; }
    }
}
