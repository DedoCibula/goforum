﻿using CookComputing.XmlRpc;
using System;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class Configuration : OperationResult
    {
        /// <summary>
        /// Url of forum to which this configuration belongs
        /// </summary>

        public string Url { get; set; }

        /// <summary>
        /// Forum system version without forum type like 3.0.9 and 4.1.4
        /// </summary>

        [XmlRpcMember("sys_version")]
        public string SystemVersion { get; set; }

        /// <summary>
        /// Tapatalk plugin version. 
        /// Plugin developers: Set "version=dev" in order to get your development environment verified by the Tapatalk Network.
        /// </summary>
        [XmlRpcMember("version")]
        public string Version { get; set; }

        /// <summary>
        /// This value is to advise the app the API Level of this mobiquo plugin is currently at.
        /// Tapatalk API is currently at Level 3 ("api_level=3").
        /// If this name/value pair is missing, the app would fall back to legacy mode for compatibility purpose.
        /// We plan to break the existing API into multiple levels so developer does not need to implement ALL the documented API for faster prototyping and improve time-to-market in phased basis.
        /// For plugin developer, please always return this key so the app will not fall back to legacy mode which may invoke undocumented functions.
        /// </summary>
        [XmlRpcMember("api_level")]
        public string ApiLevel { get; set; }

        /// <summary>
        /// false: service is not available / true: service is available.
        /// Administrator can turn on/off by adjusting a value in a configuration file.
        /// If administrator set the entire forum system in maintenance mode, this should override this setting
        /// </summary>
        [XmlRpcMember("is_open")]
        public bool IsOpen { get; set; }

        /// <summary>
        /// false: guest access is not allowed / true: guess access is allowed.
        /// Administrator can turn on/off by adjusting a value in the configuration file.
        /// If the forum system allows only login user, then the system value should over this setting
        /// </summary>
        [XmlRpcMember("guest_okay")]
        public bool AreGuestsAllowed { get; set; }

        /// <summary>
        /// Return true to indicate the plugin support report_post function.
        /// </summary>

        [XmlRpcMember("report_post")]
        public string ReportPostAvailable { get; set; }

        /// <summary>
        /// Return true to indicate the plugin support report_pm function.
        /// </summary>

        [XmlRpcMember("report_pm")]
        public string ReportPMAvailable { get; set; }

        /// <summary>
        /// Please refer to get_thread_by_post for more details.
        /// Only use this flag if the plugin is only API Level 3 but want to enable this feature.
        /// This flag will no longer needed in API Level 4
        /// </summary>
        [Obsolete]

        [XmlRpcMember("goto_post")]
        public string GoToPostAvailable { get; set; }

        /// <summary>
        /// Please refer to get_thread_by_unread for more details.
        /// Only use this flag if the plugin is only API Level 3 but want to enable this feature.
        /// </summary>
        [Obsolete]

        [XmlRpcMember("goto_unread")]
        public string GoToUnreadAvailable { get; set; }

        /// <summary>
        /// It should returns "1" or "0". If this key is missing, always assume "1".
        /// This is to indicate if the forum system support function mark_all_as_read.
        /// E.g. bbPress do not have read/unread function hence missing this functionality.
        /// </summary>

        [XmlRpcMember("mark_read")]
        public string MarkReadAvailable = "1";

        /// <summary>
        /// It should returns "1" or "0". If this key is missing, always assume "0".
        /// This is to indicate if function mark_all_as_read can accept a parameter as forum id to mark a specified forum as read.
        /// Please refer to mark_all_as_read for more details.
        /// Only use this flag if the plugin is only API Level 3 but want to enable this feature.
        /// </summary>
        [Obsolete]

        [XmlRpcMember("mark_forum")]
        public string MarkForumAvailable = "0";

        /// <summary>
        /// It should returns "1" or "0". If this key is missing, always assume "0".
        /// Please refer to reply_post and save_raw_post for more details.
        /// Only use this flag if the plugin is only API Level 3 but want to enable this feature.
        /// </summary>
        [Obsolete]

        [XmlRpcMember("no_refresh_on_post")]
        public string NoRefreshOnPostAvailable = "0";

        /// <summary>
        /// It should returns "1" or "0". If this key is missing, always assume "1".
        /// This is to indicate this forum system supports "Sub-Forum Subscription" feature.
        /// Please refer to get_subscribed_forum for more details
        /// </summary>

        [XmlRpcMember("subscribe_forum")]
        public string ForumSubscriptionAvailable = "1";

        /// <summary>
        /// It should returns "1" or "0". If this key is missing, always assume "0".
        /// This is to indicate this forum system does not support "get_latest_topic" function.
        /// Please refer to get_latest_topic for more details Please refer to get_latest_topic for more details
        /// </summary>

        [XmlRpcMember("get_latest_topic")]
        public string GetLatestTopicAvailable = "0";

        /// <summary>
        /// It should returns "1" or "0". If this key is missing, always assume "0".
        /// This is to indicate this forum system does not support get_id_by_url function.
        /// Please refer to get_id_by_url for more details
        /// </summary>
        [Obsolete]

        [XmlRpcMember("get_id_by_url")]
        public string GetIdByUrlAvailable = "0";

        /// <summary>
        /// Return true if the plugin support a delete reason parameter.
        /// Please refer to m_delete_post and m_delete_topic .
        /// </summary>

        [XmlRpcMember("delete_reason")]
        public string DeleteReasonParameterAvailable { get; set; }

        /// <summary>
        /// Returns "1" if supported.
        /// This is to indicate this forum system supports a centralized view to list all topics / posts pending to be approved.
        /// Please refer to m_get_moderate_topic for more details
        /// </summary>

        [XmlRpcMember("mod_approve")]
        public string ModApprove { get; set; }

        /// <summary>
        /// Returns "1" if supported.
        /// This is to indicate this forum system supports a centralized view to list all topics / posts that has been soft-deleted, allowing moderator to undelete topics / posts.
        /// Please refer to m_get_delete_topic for more details
        /// </summary>

        [XmlRpcMember("mod_delete")]
        public string ModDelete { get; set; }

        /// <summary>
        /// Returns "1" if supported.
        /// This is to indicate this forum system supports a centralized view to list all topics / posts that have been reported by the users and need moderator attention.
        /// Please refer to m_get_report for more details
        /// </summary>

        [XmlRpcMember("mod_report")]
        public string ModReport { get; set; }

        /// <summary>
        /// Returns "1" if guest user can search in this forum without logging in.
        /// This is helpful as the app can enable search function under guest mode
        /// </summary>

        [XmlRpcMember("guest_search")]
        public string GuestSearchAvailable { get; set; }

        /// <summary>
        /// Return 1 if plugin support anonymous login. Check function login for details.
        /// </summary>
        [Obsolete]

        [XmlRpcMember("anonymous")]
        public string AnonymousLoginAvailable { get; set; }

        /// <summary>
        /// Return "1" if guest user can see who is currently online
        /// </summary>

        [XmlRpcMember("guest_whosonline")]
        public string GuestsCanSeeWhoIsOnline { get; set; }

        /// <summary>
        /// Return "1" if search-related function like get_latest_topic / get_unread_topic / get_participated_topic / search_topic / search_post can support searchid pagination.
        /// </summary>
        [Obsolete]

        [XmlRpcMember("searchid")]
        public string SearchPaginationAvailable { get; set; }

        /// <summary>
        /// Return "1" if the plugin support get user avatar by avatar.php entry file. Please refer to avatar.php for more details
        /// </summary>
        [Obsolete]

        [XmlRpcMember("avatar")]
        public string AvatarPhpFileAvailable { get; set; }

        /// <summary>
        /// Return "1" if get_box support pagination.
        /// </summary>
        [Obsolete]

        [XmlRpcMember("pm_load")]
        public string GetBoxPaginationAvailable { get; set; }

        /// <summary>
        /// Return "1" if get_subscribed_topic support pagination.
        /// </summary>
        [Obsolete]

        [XmlRpcMember("subscribe_load")]
        public string GetSubscribedTopicsPaginationAvailable { get; set; }

        /// <summary>
        /// Minimum string length for search_topic / search_post / search within forum.
        /// </summary>

        [XmlRpcMember("min_search_length")]
        public int MinimalSearchStringLength { get; set; }

        /// <summary>
        /// Return "1" if the plugin support pm and subscribed topic unread number since last check time.
        /// For more, please check function get_inbox_stat
        /// </summary>
        [Obsolete]

        [XmlRpcMember("inbox_stat")]
        public string UnreadNumberOfPMsSinceAvailable { get; set; }

        /// <summary>
        /// Return 1 is the plugin support multi quote. Check more in get_quote_post
        /// </summary>
        [Obsolete]

        [XmlRpcMember("multi_quote")]
        public string MultiQuoteAvailable { get; set; }

        /// <summary>
        /// Forum default smilie set support. If set to "0" To indicate this forum has turned off default smilies support.
        /// </summary>
        [Obsolete]

        [XmlRpcMember("default_smilies")]
        public string DefaultSmiliesAvailable { get; set; }

        /// <summary>
        /// It should returns "1" or "0". If this key is missing, always assume "1".
        /// If it set to "0", indicate this forum does not support Unread feature.
        /// E.g. bbPress 1.1 does not have Unread feature so app side need to hide all the related functions.
        /// </summary>
        [Obsolete]

        [XmlRpcMember("can_unread")]
        public string UnreadAvailable = "1";

        /// <summary>
        /// It should returns "1" or "0". If this key is missing, always assume "1".
        /// This instructs the app to hide/show the "Announcement" tab in topic view
        /// </summary>

        [XmlRpcMember("announcement")]
        public string HidingAnnouncementTabAvailable = "1";

        /// <summary>
        /// Return 1 to indicate the plugin contains emoji package
        /// </summary>

        [XmlRpcMember("emoji")]
        public string EmojiAvailable { get; set; }

        /// <summary>
        /// Return 1 to indicate the plugin support md5 password. Like in vbulletin plugin
        /// </summary>

        [XmlRpcMember("support_md5")]
        public string SupportsMd5 { get; set; }

        /// <summary>
        /// Return 1 to indicate the plugin support sha1 password. (To be implemented in app)
        /// </summary>

        [XmlRpcMember("support_sha1")]
        public string SupportsSha1 { get; set; }

        /// <summary>
        /// Return 1 if the plugin support conversation pm (currently only Xenforo plugin support conversation pm. IPB plugin should support it later.
        /// </summary>

        [XmlRpcMember("conversation")]
        public string ConversationsAvailable { get; set; }

        /// <summary>
        /// Return 1 if the plugin support function get_forum with two parameters for description control and sub forum id filter.
        /// </summary>

        [XmlRpcMember("get_forum")]
        public string Function_GetForum_Available { get; set; }

        /// <summary>
        /// Return 1 if the plugin support function get_topic_status
        /// </summary>

        [XmlRpcMember("get_topic_status")]
        public string Function_GetTopicStatus_Available { get; set; }

        /// <summary>
        /// Return 1 if the plugin support function get_participated_forum
        /// </summary>

        [XmlRpcMember("get_participated_forum")]
        public string Function_GetParticipatedForum_Available { get; set; }

        /// <summary>
        /// Return 1 if the plugin support function get_forum_status
        /// </summary>

        [XmlRpcMember("get_forum_status")]
        public string Function_GetForumStatus_Available { get; set; }

        /// <summary>
        /// Return 1 if the plugin support function get_smilies
        /// </summary>

        [XmlRpcMember("get_smilies")]
        public string Function_GetSmilies_Available { get; set; }

        /// <summary>
        /// Return 1 if the plugin support get_online_users with forum and thread filter, and also pagination
        /// </summary>

        [XmlRpcMember("advanced_online_users")]
        public string Function_AdvancedOnlineUsers_Available { get; set; }

        /// <summary>
        /// Return 1 if the plugin support function mark_pm_unread
        /// </summary>

        [XmlRpcMember("mark_pm_unread")]
        public string Function_MarPMUnread_Available { get; set; }

        /// <summary>
        /// Return 1 if the plugin support function search
        /// </summary>

        [XmlRpcMember("advanced_search")]
        public string Function_AdvancedSearch_Available { get; set; }

        /// <summary>
        /// Return 1 if the plugin support id 'ALL' in unsubscribe_topic / unsubscribe_forum
        /// </summary>

        [XmlRpcMember("mass_subscribe")]
        public string Function_MassSubscribe_Available { get; set; }

        /// <summary>
        /// Indicate the function get_participated_topic / get_user_info / get_user_topic / get_user_reply_post support request with user id.
        /// </summary>

        [XmlRpcMember("user_id")]
        public string UserIdRequestAvailable { get; set; }

        /// <summary>
        /// Return '1' if the plugin support both soft and hard delete.
        /// </summary>

        [XmlRpcMember("advanced_delete")]
        public string AdvancedDeleteAvailable { get; set; }

        /// <summary>
        /// Return 1 if the plugin support function mark_topic_read
        /// </summary>

        [XmlRpcMember("mark_topic_read")]
        public string Function_MarkTopicRead_Available { get; set; }

        /// <summary>
        /// returns "0" if this forum system does not support First Unread feature. Assume "1" if missing.
        /// </summary>

        [XmlRpcMember("first_unread")]
        public string FirstUnreadAvailable = "1";

        /// <summary>
        /// Indicate if the plugin support function get_alert
        /// </summary>

        [XmlRpcMember("alert")]
        public string Function_GetAlert_Available { get; set; }

        /// <summary>
        /// Indicate if user can unsubscribe_topic / unsubscribe_forum with subscribed id, not topic/forum id.
        /// </summary>

        [XmlRpcMember("direct_unsubscribe")]
        public string DirectUnsubscribeAvailable { get; set; }

        /// <summary>
        /// Indicate if the plugin support function get_activity
        /// </summary>

        [XmlRpcMember("get_activity")]
        public string Function_GetActivity_Available { get; set; }

        /// <summary>
        /// Supported push type, joined with ','. Sample:ann,conv,pm,sub,like,thank,quote,newtopic,tag
        /// </summary>

        [XmlRpcMember("push_type")]
        public string PushTypeJoinedWithCommaAvailable { get; set; }

        /// <summary>
        /// return 1 if plugin support prefix update in function m_rename_topic
        /// </summary>

        [XmlRpcMember("prefix_edit")]
        public string UpdateTopicWithPrefixAvailable { get; set; }

        /// <summary>
        /// Indicate if mod can delete user's post/topic when do ban and how the content will be deleted.
        /// Optional type: soft_delete, hard_delete, unapprove, none. 'none' means mod can not update user's post/topic status when do ban
        /// </summary>

        [XmlRpcMember("ban_delete_type")]
        public string BanDeleteType { get; set; }

        /// <summary>
        /// Indicate if plugin support anonymous login
        /// </summary>

        [XmlRpcMember("anonymous_login")]
        public string AnonymousLogin2Available { get; set; }
    }
}
/*
<?xml version="1.0" encoding="UTF-8"?>
<methodResponse>
<params>
<param>
<value><struct>
<member><name>sys_version</name>
<value><string>3.0.11</string></value>
</member>
<member><name>is_open</name>
<value><boolean>1</boolean></value>
</member>
<member><name>guest_okay</name>
<value><boolean>1</boolean></value>
</member>
<member><name>api_level</name>
<value><string>3</string></value>
</member>
<member><name>version</name>
<value><string>pb30_3.7.1</string></value>
</member>
<member><name>reg_url</name>
<value><string>ucp.php?mode=register</string></value>
</member>
<member><name>disable_search</name>
<value><string>0</string></value>
</member>
<member><name>disable_latest</name>
<value><string>0</string></value>
</member>
<member><name>disable_bbcode</name>
<value><string>0</string></value>
</member>
<member><name>report_post</name>
<value><string>1</string></value>
</member>
<member><name>report_pm</name>
<value><string>1</string></value>
</member>
<member><name>mark_read</name>
<value><string>1</string></value>
</member>
<member><name>mark_forum</name>
<value><string>1</string></value>
</member>
<member><name>goto_unread</name>
<value><string>1</string></value>
</member>
<member><name>goto_post</name>
<value><string>1</string></value>
</member>
<member><name>get_latest_topic</name>
<value><string>1</string></value>
</member>
<member><name>no_refresh_on_post</name>
<value><string>1</string></value>
</member>
<member><name>get_id_by_url</name>
<value><string>1</string></value>
</member>
<member><name>mod_approve</name>
<value><string>1</string></value>
</member>
<member><name>mod_delete</name>
<value><string>0</string></value>
</member>
<member><name>mod_report</name>
<value><string>1</string></value>
</member>
<member><name>soft_delete</name>
<value><string>0</string></value>
</member>
<member><name>delete_reason</name>
<value><string>0</string></value>
</member>
<member><name>emoji</name>
<value><string>1</string></value>
</member>
<member><name>user_id</name>
<value><string>1</string></value>
</member>
<member><name>multi_quote</name>
<value><string>1</string></value>
</member>
<member><name>pm_load</name>
<value><string>1</string></value>
</member>
<member><name>subscribe_load</name>
<value><string>1</string></value>
</member>
<member><name>participated_forum</name>
<value><string>1</string></value>
</member>
<member><name>anonymous</name>
<value><string>1</string></value>
</member>
<member><name>advanced_search</name>
<value><string>1</string></value>
</member>
<member><name>alert</name>
<value><string>1</string></value>
</member>
<member><name>push</name>
<value><string>1</string></value>
</member>
<member><name>push_type</name>
<value><string>pm,sub,quote,newtopic,tag</string></value>
</member>
<member><name>inappreg</name>
<value><string>1</string></value>
</member>
<member><name>announcement</name>
<value><string>1</string></value>
</member>
<member><name>guest_search</name>
<value><string>1</string></value>
</member>
<member><name>min_search_length</name>
<value><int>3</int></value>
</member>
</struct></value>
</param>
</params>
</methodResponse>
*/