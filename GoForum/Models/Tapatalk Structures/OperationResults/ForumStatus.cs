﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class ForumStatus : ForumBase
    {

        [XmlRpcMember("forums")]
        public object[] Forums { get; set; }
    }
}
