﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class PushType
    {
        /// <summary>
        /// Push type key, valid value: ann,conv,pm,sub,like,thank,quote,newtopic,tag.
        /// </summary>

        [XmlRpcMember("name")]
        public string Name { get; set; }


        /// <summary>
        /// Status of this push type, 1 for enable, 0 for disable.
        /// </summary>

        [XmlRpcMember("value")]
        public bool Value { get; set; }
    }
}
