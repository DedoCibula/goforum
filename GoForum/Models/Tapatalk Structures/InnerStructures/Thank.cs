﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class Thank
    {
        /// <summary>
        /// Id of the user who has thanked this post.
        /// </summary>

        [XmlRpcMember("userid")]
        public string UserID { get; set; }


        /// <summary>
        /// Name of the user who has thanked this post.
        /// </summary>

        [XmlRpcMember("username")]
        public byte[] UserNameByte { get; set; }


        /// <summary>
        /// Name of the user who has thanked this post.
        /// </summary>

        public string UserName
        {
            get { return UserNameByte.GetString(); }
        }
    }
}
