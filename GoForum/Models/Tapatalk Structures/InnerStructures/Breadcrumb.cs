﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class Breadcrumb
    {
        /// <summary>
        /// 
        /// </summary>

        [XmlRpcMember("forum_id")]
        public string ForumID { get; set; }


        /// <summary>
        /// 
        /// </summary>

        [XmlRpcMember("forum_name")]
        public byte[] ForumNameByte { get; set; }


        /// <summary>
        /// 
        /// </summary>

        public string ForumName
        {
            get { return ForumNameByte.GetString(); }
        }


        /// <summary>
        /// 
        /// </summary>

        [XmlRpcMember("sub_only")]
        public bool SubOnly { get; set; }
    }
}
