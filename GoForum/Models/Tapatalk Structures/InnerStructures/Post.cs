﻿using System;
using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class Post
    {
        [XmlRpcMember("post_id")]
        public string PostID { get; set; }


        /// <summary>
        /// Remove all BBCode in title
        /// </summary>
        [XmlRpcMember("post_title")]
        public byte[] TitleByte { get; set; }


        /// <summary>
        /// Remove all BBCode in title
        /// </summary>

        public string Title
        {
            get { return TitleByte.GetString(); }
        }


        /// <summary>
        /// Characters display rules (follow the sequence): 
        /// 1) Remove all BBCode except [url], [img], [quote], [spoiler] 
        /// 2) Remove all non-displayable characters (e.g. \n, \t, white-space, etc) at the beginning AND the end of the content (Trimming) 
        /// 3) when 'return_html' was set as true, it will also return html tag <b>,<i>,<u>,<br />
        /// </summary>
        [XmlRpcMember("post_content")]
        public byte[] ContentByte { get; set; }


        /// <summary>
        /// Characters display rules (follow the sequence): 
        /// 1) Remove all BBCode except [url], [img], [quote], [spoiler] 
        /// 2) Remove all non-displayable characters (e.g. \n, \t, white-space, etc) at the beginning AND the end of the content (Trimming) 
        /// 3) when 'return_html' was set as true, it will also return html tag <b>,<i>,<u>,<br />
        /// </summary>

        public string Content
        {
            get { return ContentByte.GetString(); }
        }


        [XmlRpcMember("post_author_id")]
        public string AuthorID { get; set; }



        [XmlRpcMember("post_author_name")]
        public byte[] AuthorNameByte { get; set; }



        public string AuthorName
        {
            get { return AuthorNameByte.GetString(); }
        }


        /// <summary>
        /// return true if this user is currently online
        /// </summary>

        [XmlRpcMember("is_online")]
        public bool IsOnline { get; set; }


        /// <summary>
        /// return true if user can edit this post.
        /// </summary>

        [XmlRpcMember("can_edit")]
        public bool CanEdit { get; set; }


        /// <summary>
        /// Return topic author avatar URL
        /// </summary>

        [XmlRpcMember("icon_url")]
        public string IconUrl { get; set; }


        /// <summary>
        /// DateTime.iso8601 format. If this topic has no reply, use the topic creation time.
        /// </summary>
        [XmlRpcMember("post_time")]
        public DateTime Time { get; set; }


        /// <summary>
        /// This value (if "false") is to instruct the app do not display smilies and instead display it in text format.
        /// This is to address certain forum system allows forum admin to disable smilies across the board. If this flag is missing, assume "true"
        /// </summary>

        [XmlRpcMember("allow_smilies")]
        public bool AllowSmilies { get; set; }
    }
}
