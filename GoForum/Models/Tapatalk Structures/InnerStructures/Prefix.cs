﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class Prefix
    {

        [XmlRpcMember("prefix_id")]
        public string ID { get; set; }



        [XmlRpcMember("prefix_display_name")]
        public byte[] DisplayNameByte { get; set; }



        public string DisplayName
        {
            get { return DisplayNameByte.GetString(); }
        }
    }
}
