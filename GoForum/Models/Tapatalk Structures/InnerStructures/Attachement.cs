﻿using CookComputing.XmlRpc;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class Attachement
    {
        /// <summary>
        /// return "image", "pdf" or "other"
        /// </summary>
        [XmlRpcMember("content_type")]
        public string ContentType { get; set; }


        /// <summary>
        /// if content type = "image"), use absolute path (optional: if not presented, use "url" to load thumbnail instead)
        /// </summary>

        [XmlRpcMember("thumbnail_url")]
        public string ThumbnailUrl { get; set; }


        /// <summary>
        /// URL of the attachment source.
        /// </summary>

        [XmlRpcMember("url")]
        public string Url { get; set; }
    }
}
