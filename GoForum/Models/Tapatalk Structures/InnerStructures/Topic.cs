﻿using System;
using CookComputing.XmlRpc;
using System.ComponentModel;
using GoForum.Models.Tapatalk_Structures.InnerStructures;

namespace GoForum.Models.Tapatalk_Structures
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class Topic
    {

        [XmlRpcMember("forum_id")]
        public string ForumID { get; set; }



        [XmlRpcMember("topic_id")]
        public string ID { get; set; }



        [XmlRpcMember("topic_title")]
        public byte[] TitleByte { get; set; }



        public string Title
        {
            get { return TitleByte.GetString(); }
        }



        [XmlRpcMember("prefix")]
        public byte[] PrefixByte { get; set; }



        public string Prefix
        {
            get { return PrefixByte.GetString(); }
        }



        [XmlRpcMember("topic_author_id")]
        public string AuthorID { get; set; }



        [XmlRpcMember("topic_author_name")]
        public byte[] AuthorNameByte { get; set; }



        public string AuthorName
        {
            get { return AuthorNameByte.GetString(); }
        }


        /// <summary>
        /// return true if this thread has been subscribed by this user.
        /// </summary>

        [XmlRpcMember("is_subscribed")]
        public bool IsSubsribed { get; set; }


        /// <summary>
        /// returns false if the subscription feature is turned off
        /// </summary>

        [XmlRpcMember("can_subscribe")]
        public bool CanSubscribe { get; set; }


        /// <summary>
        /// return true if this thread has been closed. 
        /// If this thread is closed, it should not allow reply or edit unless can_reply (thread) or can_edit (post) are set to "true" explicitly
        /// </summary>

        [XmlRpcMember("is_closed")]
        public bool IsClosed { get; set; }


        /// <summary>
        /// Return topic author avatar URL
        /// </summary>

        [XmlRpcMember("icon_url")]
        public string IconUrl { get; set; }


        /// <summary>
        /// dateTime.iso8601 format. If this topic has no reply, use the topic creation time.
        /// </summary>

        [XmlRpcMember("last_reply_time")]
        public DateTime LastReplyTime { get; set; }


        /// <summary>
        /// total number of reply in this topic. If this is no reply in this return, return 0.
        /// </summary>

        [XmlRpcMember("reply_number")]
        public int Replies { get; set; }


        /// <summary>
        /// return true if this topic contains new post since user last login
        /// </summary>

        [XmlRpcMember("new_post")]
        public bool ContainsNewPost { get; set; }


        /// <summary>
        /// total number of view in this topic
        /// </summary>

        [XmlRpcMember("view_number")]
        public int Views { get; set; }


        /// <summary>
        /// Characters display rules (should follow this sequence): 
        /// 1) Remove all BBCode except [ur], [img]. 
        /// 2) Convert "[url http://...]http://…..[/url]" to "[url]". 
        /// 3) Convert "[img]http://…..[/img]" to "[img]". 
        /// 4) Remove "Last edited by..." tag at the end. 
        /// 5) Remove all non-displayable characters (e.g. \n, \t, etc). 
        /// 6) Remove all whitespace, /n and /r at the beginning and ending of the content. 
        /// 7) return only first 200 characters
        /// </summary>

        [XmlRpcMember("short_content")]
        public byte[] ShortContentByte { get; set; }


        /// <summary>
        /// Characters display rules (should follow this sequence): 
        /// 1) Remove all BBCode except [ur], [img]. 
        /// 2) Convert "[url http://...]http://…..[/url]" to "[url]". 
        /// 3) Convert "[img]http://…..[/img]" to "[img]". 
        /// 4) Remove "Last edited by..." tag at the end. 
        /// 5) Remove all non-displayable characters (e.g. \n, \t, etc). 
        /// 6) Remove all whitespace, /n and /r at the beginning and ending of the content. 
        /// 7) return only first 200 characters
        /// </summary>

        public string ShortContent
        {
            get { return ShortContentByte.GetString(); }
        }



        [XmlRpcMember("is_sticky")]
        public bool IsSticky { get; set; }



        [XmlRpcMember("is_announcement")]
        public bool IsAnnouncement { get; set; }


        public TopicMode TopicType
        {
            get
            {
                if (IsAnnouncement)
                    return TopicMode.ANN;

                if (IsSticky)
                    return TopicMode.TOP;

                return TopicMode.NORMAL;
            }
        }
    }
}
