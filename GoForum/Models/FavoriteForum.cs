﻿using System;

namespace GoForum.Models
{
    [Serializable]
    public class FavoriteForum
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string MobiquoUrl { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public FavoriteForum()
        {
        }

        public FavoriteForum(string name, string url, string mobiquoUrl, string username, string password)
        {
            Name = name;
            Url = url;
            MobiquoUrl = mobiquoUrl;
            Username = username;
            Password = password;
        }

        public override bool Equals(object obj)
        {
            var forum = obj as FavoriteForum;
            if (forum != null)
            {
                return forum.MobiquoUrl == MobiquoUrl;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return MobiquoUrl.GetHashCode();
        }
    }
}