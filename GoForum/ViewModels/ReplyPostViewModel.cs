﻿using System;
using System.Windows;
using System.Collections.Generic;
using Microsoft.Phone.Info;
using GoForum.Services;
using GoForum.Models.Tapatalk_Structures;
using System.Threading.Tasks;
using System.Windows.Navigation;
using GoForum.Helpers;
using GoForum.Resx;

namespace GoForum.ViewModels
{
    public class ReplyPostViewModel : ViewModelBase
    {
        public IPostService PostsService { get; set; }

        private NavigationService _navigationService;

        private bool _submitEnabled;

        public bool SubmitEnabled
        {
            get { return _submitEnabled; }

            set
            {
                if (_submitEnabled == value) return;

                _submitEnabled = value;
                RaisePropertyChanged("SubmitEnabled");
            }
        }



        private bool _cancelEnabled;

        public bool CancelEnabled
        {
            get { return _cancelEnabled; }

            set
            {
                if (_cancelEnabled == value) return;

                _cancelEnabled = value;
                RaisePropertyChanged("CancelEnabled");
            }
        }


        private string _body;

        public string Body
        {
            get { return _body; }

            set
            {
                if (_body == value) return;

                _body = value;
                RaisePropertyChanged("Body");
            }
        }


        private string _subject;

        public string Subject
        {
            get { return _subject; }

            set
            {
                if (_subject == value) return;

                _subject = value;
                RaisePropertyChanged("Subject");
            }
        }

        public ReplyPostViewModel()
        {
            PostsService = IoCContainer.Get<IPostService>(App.CurrentForumUrl);
        }

        public void InitSubsribers()
        {
            var replySubsriber = Events<ReplyPost>.Subscribe(t =>
            {
                if (t.Result)
                {
                    _navigationService.Navigate(new Uri("/Posts/" + StateHelper.GetFromCurrentState(States.TOPIC_ID, 0), UriKind.Relative));
                }
                else
                {
                    ProgressIndicatorHelper.TryHideProgressIndicator();
                    MessageBox.Show(t.ResultText);
                    CancelEnabled = true;
                    SubmitEnabled = true;
                }
            }, e =>
            {
                ProgressIndicatorHelper.TryHideProgressIndicator();
                MessageBox.Show(e.Message);
                CancelEnabled = true;
                SubmitEnabled = true;
            });

            StoreSubscribers(replySubsriber);
        }

        public void Load(IDictionary<string, string> queryString, NavigationService service)
        {
            var subject = StateHelper.GetFromCurrentState(States.REPLYPOST_SUBJECT, "");
            if (!subject.StartsWith("Re:", StringComparison.CurrentCultureIgnoreCase))
            {
                subject = "Re: " + subject;
            }
            Subject = subject;

            var signature = SettingsHelper.GetSetting(Settings.SIGNATURE, "Sent from my Windows Phone");
            if (signature != "")
                Body = string.Format("\r\r{0}", signature);

            _navigationService = service;

            CancelEnabled = true;
            SubmitEnabled = true;
        }

        public override void Dispose()
        {
            Events<ReplyPost>.Dispose(Subscribers);
        }

        public void Submit()
        {
            CancelEnabled = false;
            SubmitEnabled = false;
            Task.Run(() => PostsService.ReplyPost(Int32.Parse(App.CurrentThread.ForumID), Int32.Parse(App.CurrentThread.TopicID), Subject, Body.Replace("\r", "\r\n")));
        }
    }
}
