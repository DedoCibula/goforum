﻿using System.Collections.ObjectModel;
using System.Windows;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Models.Tapatalk_Structures.InnerStructures;
using GoForum.Services;
using System.Threading.Tasks;

namespace GoForum.ViewModels
{
    public class TopicsViewModel : ViewModelBase
    {
        private int _timesLoaded;
        private int _newStart;
        private int _newEnd;
        private int _totalTopicsCount;
        private readonly object _topicLock = new object();

        public ITopicService TopicService { get; set; }

        public ObservableCollection<Topic> Topics { get; set; }

        public int ForumID { get; set; }

        private Visibility _fetchNew;

        public Visibility FetchNewVisible
        {
            get { return _fetchNew; }

            set
            {
                if (_fetchNew == value) return;

                _fetchNew = value;
                RaisePropertyChanged("FetchNewVisible");
            }
        }

        private bool _fetchNewEnabled;

        public bool FetchNewEnabled
        {
            get { return _fetchNewEnabled; }

            set
            {
                if (_fetchNewEnabled == value) return;

                _fetchNewEnabled = value;
                RaisePropertyChanged("FetchNewEnabled");
            }
        }


        public TopicsViewModel(string url)
        {
            FetchNewVisible = Visibility.Collapsed;
            TopicService = IoCContainer.Get<ITopicService>(url);
            Topics = new ObservableCollection<Topic>();
        }

        public void InitSubsribers()
        {
            _timesLoaded = 0;

            var topicsSubsriber = Events<GetTopic>.Subscribe(t =>
                {
                    lock (_topicLock)
                    {
                        _timesLoaded++;

                        if (t.Result)
                        {
                            foreach (var topic in t.Topics)
                            {
                                Topics.Add(topic);
                            }

                            if (t.TopicMode == TopicMode.NORMAL)
                            {
                                _totalTopicsCount = t.TopicsCount;

                                _newStart += 20;
                                _newEnd = _newStart + 19;

                                if (_newEnd > _totalTopicsCount)
                                {
                                    _newEnd = _totalTopicsCount;
                                }

                                if (_totalTopicsCount <= 20 || _newStart > _totalTopicsCount || _newEnd < _newStart)
                                {
                                    FetchNewVisible = Visibility.Collapsed;
                                }
                                else
                                {
                                    FetchNewVisible = Visibility.Visible;
                                }

                                FetchNewEnabled = true;
                            }
                        }
                        else
                        {
                            MessageBox.Show(t.ResultText);
                        }

                        if (_timesLoaded > 2)
                        {
                            ProgressIndicatorHelper.TryHideProgressIndicator();
                        }
                    }
                }, e =>
                {
                    MessageBox.Show(e.Message);
                });

            var subscribeTopicSubscriber = Events<SubscribeTopic>.Subscribe(t =>
                {
                    if (!t.Result)
                    {
                        MessageBox.Show("You have already subsribed to this topic");
                    }
                    ProgressIndicatorHelper.TryHideProgressIndicator();
                },
                ex =>
                {
                    MessageBox.Show(ex.Message);
                    ProgressIndicatorHelper.TryHideProgressIndicator();
                });

            StoreSubscribers(topicsSubsriber);
            StoreSubscribers(subscribeTopicSubscriber);
        }

        public void GetTopics(int forumID, TopicMode mode)
        {
            ForumID = forumID;

            Task.Run(() => TopicService.GetTopic(forumID, null, null, mode));
        }

        public void GetNextTopics()
        {
            FetchNewEnabled = false;
            Task.Run(() => TopicService.GetTopic(ForumID, _newStart, _newEnd, TopicMode.NORMAL));
        }

        public override void Dispose()
        {
            Events<GetTopic>.Dispose(Subscribers);
            Events<SubscribeTopic>.Dispose(Subscribers);
        }
    }
}
