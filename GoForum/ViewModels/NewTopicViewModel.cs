﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using GoForum.Helpers;
using GoForum.Resx;
using Microsoft.Phone.Info;
using GoForum.Services;
using GoForum.Models.Tapatalk_Structures;
using System.Windows.Navigation;
using System.Threading.Tasks;

namespace GoForum.ViewModels
{
    public class NewTopicViewModel : ViewModelBase
    {
        public ITopicService TopicService { get; set; }

        private NavigationService _navigationService;

        public int ParentID { get; set; }

        private bool _submitEnabled;

        public bool SubmitEnabled
        {
            get { return _submitEnabled; }

            set
            {
                if (_submitEnabled == value) return;

                _submitEnabled = value;
                RaisePropertyChanged("SubmitEnabled");
            }
        }

        private bool _cancelEnabled;

        public bool CancelEnabled
        {
            get { return _cancelEnabled; }

            set
            {
                if (_cancelEnabled == value) return;

                _cancelEnabled = value;
                RaisePropertyChanged("CancelEnabled");
            }
        }


        private string _body;

        public string Body
        {
            get { return _body; }

            set
            {
                if (_body == value) return;

                _body = value;
                RaisePropertyChanged("Body");
            }
        }


        private string _subject;

        public string Subject
        {
            get { return _subject; }

            set
            {
                if (_subject == value) return;

                _subject = value;
                RaisePropertyChanged("Subject");
            }
        }

        public NewTopicViewModel()
        {
            TopicService = IoCContainer.Get<ITopicService>(App.CurrentForumUrl);
            CancelEnabled = true;
            SubmitEnabled = true;
        }

        public void InitSubsribers()
        {
            var subsriber = Events<NewTopic>.Subscribe(t =>
            {
                if (t.Result)
                {
                    var uriStr = string.Format("/Posts/{0}", t.TopicID);
                    _navigationService.Navigate(new Uri(uriStr, UriKind.Relative));
                }
                else
                {
                    ProgressIndicatorHelper.TryHideProgressIndicator();
                    MessageBox.Show(t.ResultText);
                    CancelEnabled = true;
                    SubmitEnabled = true;
                }
            }, e =>
            {
                ProgressIndicatorHelper.TryHideProgressIndicator();
                MessageBox.Show(e.Message);
                CancelEnabled = true;
                SubmitEnabled = true;
            });

            StoreSubscribers(subsriber);
        }

        public void Load(IDictionary<string,string> queryString, NavigationService service)
        {
            if (queryString.Count == 1)
            {
                ParentID = Convert.ToInt32(queryString["parentID"]);
            }

            var signature = SettingsHelper.GetSetting(Settings.SIGNATURE, "Sent from my Windows Phone");
            if (signature != "")
                Body = string.Format("\r\r{0}", signature);

            _navigationService = service;
        }

        public override void Dispose()
        {
            Events<NewTopic>.Dispose(Subscribers);
        }

        public void Submit()
        {
            CancelEnabled = false;
            SubmitEnabled = false;
            Task.Run(() => TopicService.NewTopic(ParentID, Subject, Body.Replace("\r", "\r\n"), null, null, null));
        }
    }
}
