﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Models.Tapatalk_Structures.InnerStructures;
using GoForum.Services;
using GoForum;
using System.Threading.Tasks;

namespace GoForum.ViewModels
{
    public class PostsViewModel : ViewModelBase
    {
        private Thread _currentThread;
        private Paginator _paginator;

        public IPostService PostsService { get; set; }

        public ObservableCollection<Post> Posts { get; set; }


        public Thread CurrentThread
        {
            get { return _currentThread; }

            set
            {
                if (_currentThread == value) return;

                _currentThread = value;
                RaisePropertyChanged("CurrentThread");
            }
        }

        public Paginator Paginator
        {
            get
            {
                return _paginator;
            }
            set
            {
                if (_paginator == value) return;

                _paginator = value;
                RaisePropertyChanged("Paginator");
            }
        }

        public PostsViewModel(string url)
        {
            PostsService = IoCContainer.Get<IPostService>(url);
            Paginator = IoCContainer.Get<Paginator>();
            Posts = new ObservableCollection<Post>();
        }

        public void InitSubsribers()
        {
            var threadSubsriber = Events<Thread>.Subscribe(t =>
                {
                    CurrentThread = t;

                    var tmp = IoCContainer.Get<Paginator>();
                    tmp.TotalItems = CurrentThread.TotalPosts;
                    tmp.PageSize = Paginator.PageSize;
                    tmp.Current = Paginator.Current;

                    Paginator = tmp;

                    Posts.Clear();

                    foreach (var post in t.Posts)
                    {
                        Posts.Add(post);
                    }
                }, e =>
                {
                });

            StoreSubscribers(threadSubsriber);
        }


        public void GetThread(int TopicId)
        {
            Task.Run(() => PostsService.GetThread(TopicId, 0, 10, false));
        }

        public void GetThread(int Forumid, int start, int end)
        {
            Task.Run(() => PostsService.GetThread(Forumid, start, end, true));
        }

        public override void Dispose()
        {
            Events<Thread>.Dispose(Subscribers);
        }
    }
}
