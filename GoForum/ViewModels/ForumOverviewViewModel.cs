﻿using System.Collections.ObjectModel;
using System.Windows;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Models.Tapatalk_Structures.InnerStructures;
using GoForum.Services;
using System.Threading.Tasks;

namespace GoForum.ViewModels
{
    public class ForumOverviewViewModel : ViewModelBase
    {  
        private readonly object _topicLock = new object();

        public ITopicService TopicService { get; set; }

        public ObservableCollection<GetSubscribedTopics> Categories { get; set; }

        public ObservableCollection<Topic> Topics { get; set; }

        public int ForumID { get; set; }

        private Visibility _fetchNew;

        public Visibility FetchNewVisible
        {
            get { return _fetchNew; }

            set
            {
                if (_fetchNew == value) return;

                _fetchNew = value;
                RaisePropertyChanged("FetchNewVisible");
            }
        }

        private bool _fetchNewEnabled;

        public bool FetchNewEnabled
        {
            get { return _fetchNewEnabled; }

            set
            {
                if (_fetchNewEnabled == value) return;

                _fetchNewEnabled = value;
                RaisePropertyChanged("FetchNewEnabled");
            }
        }


        public ForumOverviewViewModel(string url)
        {
            FetchNewVisible = Visibility.Collapsed;
            TopicService = IoCContainer.Get<ITopicService>(url);
            Topics = new ObservableCollection<Topic>();
        }

        public void InitSubsribers()
        {

            var topicsSubsriber = Events<GetSubscribedTopics>.Subscribe(t =>
                {
                    lock (_topicLock)
                    {
                        if (t.Result)
                        {
                            foreach (var topic in t.Topics)
                            {
                                Topics.Add(topic);
                            }
                        }
                        else
                        {
                            MessageBox.Show(t.ResultText);
                        }

                        ProgressIndicatorHelper.TryHideProgressIndicator();
                    }
                }, e =>
                {
                    MessageBox.Show(e.Message);
                });

            StoreSubscribers(topicsSubsriber);
        }

        public void GetSubscriptions()
        {
            Task.Run(() => TopicService.GetSubscribedTopics(0, 20));
        }

        public override void Dispose()
        {
            Events<GetSubscribedTopics>.Dispose(Subscribers);
        }
    }
}
