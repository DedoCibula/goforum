﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Services;
using System.Threading.Tasks;

namespace GoForum.ViewModels
{
    public class CategoriesViewModel : ViewModelBase
    {
        public IForumService ForumService { get; set; }

        public ObservableCollection<ForumObj> Categories { get; set; }

        public CategoriesViewModel(string url)
        {
            ForumService = IoCContainer.Get<IForumService>(url);
            Categories = new ObservableCollection<ForumObj>();
        }

        public void InitSubsribers()
        {
            var categoriesSubsriber = Events<ForumObj[]>.Subscribe(t =>
            {
                Categories.Clear();

                foreach (var category in t)
                {
                    Categories.Add(category);
                }
            }, e =>
                {

                });

            StoreSubscribers(categoriesSubsriber);
        }

        public void GetForums(int? Forumid = null)
        {
            Task.Run(() => ForumService.GetForums(true, Forumid));
        }

        public override void Dispose()
        {
            Events<ForumObj[]>.Dispose(Subscribers);
        }
    }
}
