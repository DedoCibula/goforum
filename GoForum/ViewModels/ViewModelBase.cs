﻿using System;
using System.Collections.Generic;

namespace GoForum.ViewModels
{
    public abstract class ViewModelBase : ObservableObject
    {
        protected IList<IDisposable> Subscribers { get; set; }

        protected ViewModelBase()
        {
            Subscribers = new List<IDisposable>();
        }


        /// <summary>
        /// Call to add subscribers into existing.
        /// </summary>
        /// <param name="subscribers">created observers</param>
        protected void StoreSubscribers(params IDisposable[] subscribers)
        {
            foreach (var disposable in subscribers)
            {
                Subscribers.Add(disposable);
            }
        }


        /// <summary>
        /// Dispose every service you have used here, passing it Subscribers property as parameter
        /// </summary>
        public abstract void Dispose();
    }
}
