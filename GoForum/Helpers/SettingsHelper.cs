﻿using System.IO.IsolatedStorage;

namespace GoForum.Helpers
{
    public static class SettingsHelper
    {
        static SettingsHelper()
        {
            _settings = IsolatedStorageSettings.ApplicationSettings;
        }

        private static readonly IsolatedStorageSettings _settings;

        public static T GetSetting<T>(string key, T defaultValue)
        {
            key = key.ToLower();

            if (_settings.Contains(key))
            {
                var setting = _settings[key];

                if (setting.GetType() != typeof(T) || setting == null)
                {
                    return defaultValue;
                }

                return (T)setting;
            }

            return defaultValue;
        }

        public static void SetSetting<T>(string key, T value)
        {
            key = key.ToLower();

            if (_settings.Contains(key))
            {
                _settings[key] = value;
            }
            else
            {
                _settings.Add(key, value);
            }
        }
    }
}
