﻿using System;
using System.Net;
using System.Threading;

namespace GoForum
{
    /// <summary>
    /// From http://pieterderycke.wordpress.com/2011/05/23/adding-synchronous-methods-to-webrequest-on-windows-phone-7/
    /// </summary>
    public static class WebRequestHelper
    {
        public static WebResponse GetResponse(this WebRequest request, TimeSpan timeout)
        {
            var autoResetEvent = new AutoResetEvent(false);

            IAsyncResult asyncResult = request.BeginGetResponse(r => autoResetEvent.Set(), null);

            autoResetEvent.WaitOne(timeout);

            return request.EndGetResponse(asyncResult);
        }
    }
}
