﻿using System;
using System.Net;
using CookComputing.XmlRpc;

namespace GoForum
{
    public static class ProxyHelper
    {
        public static void SetCookies(this IXmlRpcProxy proxy, Cookie[] cookies)
        {
            foreach (var cookie in cookies)
            {
                proxy.CookieContainer.SetCookies(new Uri(proxy.Url), cookie.ToString());
            }
        }
    }
}
