﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using System.Reactive.Concurrency;

namespace GoForum
{
    public static class ProgressIndicatorHelper
    {
        private static DependencyObject actualObject = null;

        public static void ShowProgressIndicator(this DependencyObject page, string text)
        {
            SystemTray.SetIsVisible(page, true);
            var indicator = new ProgressIndicator { IsIndeterminate = true, IsVisible = true, Text = text };
            Deployment.Current.Dispatcher.BeginInvoke(
                () =>
                    SystemTray.SetProgressIndicator(page, indicator));

            actualObject = page;
        }

        public static void TryHideProgressIndicator()
        {
            if (actualObject != null)
            {
                try
                {
                    Deployment.Current.Dispatcher.BeginInvoke(
                    () =>
                        SystemTray.SetProgressIndicator(actualObject, null));
                }
                catch (Exception)
                { 
                }
            }
        }


        public static void HideProgressIndicator(this DependencyObject page)
        {
            Deployment.Current.Dispatcher.BeginInvoke(
                () =>
                    SystemTray.SetProgressIndicator(page, null));
        }
    }
}
