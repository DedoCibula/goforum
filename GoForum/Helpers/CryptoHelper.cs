﻿using System;
using System.Security.Cryptography;

using MD5CryptoService;

namespace GoForum.Helpers
{
    public class CryptoHelper
    {
        public static string GetMd5Hash(string input)
        {
            var hash = new MD5CryptoServiceProvider().ComputeHash(input.GetBytes());
            return BitConverter.ToString(hash).Replace("-", "");
        }


        public static string GetSha1Hash(string input)
        {
            SHA1Managed s = new SHA1Managed();
            s.ComputeHash(input.GetBytes());
            return BitConverter.ToString(s.Hash).Replace("-", "");
        }
    }
}