﻿using System;
using System.IO.IsolatedStorage;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GoForum.Helpers
{
    public static class FileHelper
    {
        private static IsolatedStorageFile _fileStorage;

        static FileHelper()
        {
            _fileStorage = IsolatedStorageFile.GetUserStoreForApplication();
        }
    }
}
