﻿using System.Text;

namespace GoForum
{
    public static class EncodingHelper
    {
        public static string GetString(this byte[] text)
        {
            if (text != null && text.Length > 0)
            {
                return Encoding.UTF8.GetString(text, 0, text.Length);
            }

            return string.Empty;
        }


        public static byte[] GetBytes(this string input)
        {
            if (input != null)
            {
                return Encoding.UTF8.GetBytes(input);
            }

            return new byte[0];
        }
    }
}
