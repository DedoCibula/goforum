﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Shell;

namespace GoForum.Helpers
{
    public static class StateHelper
    {
        public static T GetFromCurrentState<T>(string key, T defaultValue)
        {
            if (PhoneApplicationService.Current.State.ContainsKey(key))
            {
                try
                {
                    var typed = (T)PhoneApplicationService.Current.State[key];

                    return typed;
                }
                catch (Exception)
                {
                    return defaultValue;
                }
            }
            else
            {
                return defaultValue;
            }
        }

        public static void SetToCurrentState<T>(T obj, string key)
        {
            PhoneApplicationService.Current.State[key] = obj;
        }
    }
}
