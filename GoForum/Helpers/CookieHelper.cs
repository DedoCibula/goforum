﻿using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace GoForum
{
    public static class CookieHelper
    {
        /// <summary>
        /// Parses cookies.
        /// </summary>
        /// <param name="headerSetCookie"></param>
        /// <returns></returns>
        public static Cookie[] ParseCookies(this string headerSetCookie, bool returnEmptyValues = false)
        {
            var lines = Regex.Split(headerSetCookie, ",  ");

            return lines.Select(line => line.ToCookie(returnEmptyValues)).ToArray();
        }


        private static Cookie ToCookie(this string source, bool returnEmptyValues = false)
        {
            var cookieParts = Regex.Split(source, "; ").Select(s => s.Split('='));

            var kv = cookieParts.First();

            Cookie cookie;

            if (!returnEmptyValues)
            {
                cookie = new Cookie(kv[0], kv[1]);
            }
            else
            {
                cookie = new Cookie(kv[0], string.Empty);
            }

            foreach (var cookiePart in cookieParts.Skip(1))
            {
                switch (cookiePart[0])
                {
                    case "expires":
                        cookie.Expires = DateTime.Parse(cookiePart[1]);
                        break;
                    case "path":
                        cookie.Path = cookiePart[1];
                        break;
                    case "domain":
                        cookie.Domain = cookiePart[1];
                        break;
                    case "HttpOnly":
                        cookie.HttpOnly = true;
                        break;
                }
            }
            return cookie;
        }
    }
}
