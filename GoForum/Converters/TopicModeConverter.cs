﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using GoForum.Models.Tapatalk_Structures.InnerStructures;

namespace GoForum.Converters
{
    public class TopicModeConverter : IValueConverter
    {
        private static bool isLightTheme;

        static TopicModeConverter()
        {
            isLightTheme = (Visibility)Application.Current.Resources["PhoneLightThemeVisibility"] == Visibility.Visible;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var topicMode = (TopicMode)value;

            string uri = string.Format("/Assets/Icons/{0}/", (isLightTheme) ? "light" : "dark");

            switch (topicMode)
            { 
                case TopicMode.ANN:
                    uri += "bolt.png";
                    break;

                case TopicMode.TOP:
                    uri += "pushpin.png";
                    break;

                default:
                    uri = "";
                    break;
            }

            if (!string.IsNullOrEmpty(uri))
            {
                var absoluteUri = new Uri(uri, UriKind.Relative);
                return new BitmapImage(absoluteUri);
            }
            else
            {
                return new BitmapImage();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
