﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace GoForum.Converters
{
    public class ImageConverter : IValueConverter
    {
        private static BitmapImage defaultAvatar;

        static ImageConverter()
        {
            var isLightTheme = (Visibility)Application.Current.Resources["PhoneLightThemeVisibility"] == Visibility.Visible;
            var resourceUri = string.Format("/Assets/Icons/{0}/Avatar.png", (isLightTheme) ? "light" : "dark");
            defaultAvatar = new BitmapImage(new Uri(resourceUri, UriKind.Relative));
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var stringUri = (string)value;
            if (string.IsNullOrEmpty(stringUri))
            {
                return defaultAvatar;
            }

            var uri = new Uri(stringUri, UriKind.Absolute);
            return new BitmapImage(uri);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
