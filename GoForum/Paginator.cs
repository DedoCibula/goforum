﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoForum
{
    public class Paginator
    {
        #region Variables

        private int _current = 1;
        private int _pageSize = 10;
        private int _totalItems;

        #endregion

        #region Properties

        public int Current
        {
            get
            {
                return _current;
            }
            set
            {
                _current = Math.Min(Math.Max(value, 1), PageCount);
            }
        }

        public bool IsFirst
        {
            get { return (Current == 1); }
        }

        public bool IsLast
        {
            get { return (Current == PageCount); }
        }

        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = Math.Max(value, 1);
            }
        }

        public int PageCount
        {
            get
            {
                return TotalItems / PageSize + 1;
            }
        }

        public int Start
        {
            get
            {
                return (Current - 1) * PageSize;
            }
        }

        public int End
        {
            get
            {
                return Math.Min(Current * PageSize - 1, TotalItems);
            }
        }

        public int TotalItems
        {
            get
            {
                return _totalItems;
            }
            set
            {
                _totalItems = Math.Max(value, 0);

                
            }
        }

        #endregion

        #region Methods

        public void First()
        {
            Current = 1;
        }
        
        public void Previous()
        {
            Current -= 1;
        }
        
        public void Next()
        {
            Current += 1;
        }

        public void Last()
        {
            Current = PageCount;
        }

        #endregion
    }
}
