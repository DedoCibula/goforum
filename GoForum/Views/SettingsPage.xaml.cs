﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using GoForum.Helpers;
using GoForum.Resx;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace GoForum.Views
{
    public partial class SettingsPage : PhoneApplicationPage
    {
        public SettingsPage()
        {
            InitializeComponent();
            Loaded += SettingsPage_Loaded;
        }

        void SettingsPage_Loaded(object sender, RoutedEventArgs e)
        {
            SignatureBox.Text = SettingsHelper.GetSetting(Settings.SIGNATURE, "Sent from my Windows Phone");
        }

        private void ok_Click(object sender, EventArgs e)
        {
            SettingsHelper.SetSetting(Settings.SIGNATURE, SignatureBox.Text);
            NavigationService.GoBack();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}