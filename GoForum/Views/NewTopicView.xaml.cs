﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Info;
using GoForum.ViewModels;

namespace GoForum.Views
{
    public partial class NewTopicView : PhoneApplicationPage
    {
        private NewTopicViewModel ViewModel
        {
            get { return (NewTopicViewModel)DataContext; }
            set { DataContext = value; }
        }

        public NewTopicView()
        {
            InitializeComponent();
            ViewModel = IoCContainer.Get<NewTopicViewModel>(App.CurrentForumUrl);
            Loaded += NewTopic_Loaded;
        }

        void NewTopic_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.Load(NavigationContext.QueryString, NavigationService);
            ViewModel.InitSubsribers();
            Subject.Focus();
        }

        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            this.ShowProgressIndicator("Creating new topic");
            ViewModel.Submit();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}