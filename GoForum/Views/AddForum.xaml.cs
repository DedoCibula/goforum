﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using GoForum.Models;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Services;
using GoForum.Services.Forum;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using GoForum.Helpers;

namespace GoForum.Views
{
    public partial class AddForum : PhoneApplicationPage
    {
        private FavoriteForumProvider _favForumProvider;

        private MobiquoDiscovery _discovery;
        private IDisposable _confSubsriber;
        private IDisposable _loginSubsriber;

        private bool _requestInProgress;

        public AddForum()
        {
            _favForumProvider = new FavoriteForumProvider();
            _discovery = new MobiquoDiscovery();
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CrdBox.Visibility = Visibility.Collapsed;
            _requestInProgress = true;
            ForumUrlButton.IsEnabled = false;

            this.ShowProgressIndicator("Finding forum...");

            var text = ForumUrlBox.Text;

            Task.Run(() =>
                     {
                         try
                         {
                             var canonizedUrl = _discovery.CanonizeUrl(text);
                             if (_discovery.CheckIfForumHasPlugin(canonizedUrl, true))
                             {
                                 Dispatcher.BeginInvoke(() => CrdBox.Visibility = Visibility.Visible);
                             }
                             else
                             {
                                 Dispatcher.BeginInvoke(() => MessageBox.Show(string.Format("We're sorry, no compatible forum was found on given URL ({0})!", canonizedUrl)));
                             }
                         }
                         catch (CannonizationException)
                         {
                             Dispatcher.BeginInvoke(() => MessageBox.Show(string.Format("We're sorry, no compatible forum was found on given URL ({0})!", ForumUrlBox.Text)));
                         }
                         this.HideProgressIndicator();
                         Dispatcher.BeginInvoke(() => ForumUrlButton.IsEnabled = false);
                         _requestInProgress = false;
                     });
        }

        private void ForumUrlBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!_requestInProgress)
            {
                CrdBox.Visibility = Visibility.Collapsed;
                ForumUrlButton.IsEnabled = true;
            }
        }

        private void SignButton_OnClick(object sender, RoutedEventArgs e)
        {
            var username = UsernameTextBox.Text;
            var password = PasswordTextBox.Password;
            var forumUrl = ForumUrlBox.Text;
            var forumName = ForumNameTextBox.Text;

            string canonizedUrl = "";
            string mobiquioUrl = "";

            this.ShowProgressIndicator("Trying to log in...");

            SignButton.IsEnabled = false;

            _confSubsriber = Events<Configuration>.Subscribe(
                cnf => IoCContainer.Get<IUserService>(mobiquioUrl).Login(username, password),
                ex =>
                {
                    MessageBox.Show(ex.Message);
                    SignButton.IsEnabled = true;
                });

            _loginSubsriber = Events<Login>.Subscribe(
                login =>
                {
                    if (login.Result)
                    {
                        var forum = new FavoriteForum(forumName, canonizedUrl, mobiquioUrl, username, password);
                        _favForumProvider.Add(forum);
                        MessageBox.Show("Login was succesful and forum was added!");
                        NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.RelativeOrAbsolute));
                    }
                    else
                    {
                        MessageBox.Show(login.ResultText);
                        SignButton.IsEnabled = true;
                    }
                },
                ex =>
                {
                    MessageBox.Show(ex.Message);
                    SignButton.IsEnabled = true;
                });

            Task.Run(() =>
                     {
                         try
                         {
                             canonizedUrl = _discovery.CanonizeUrl(forumUrl);
                             mobiquioUrl = canonizedUrl + "mobiquo/mobiquo.php";
                             IoCContainer.Get<IForumService>(mobiquioUrl).GetConfiguration();
                         }
                         catch (CannonizationException)
                         {
                             Dispatcher.BeginInvoke(() => MessageBox.Show("Login request failed."));
                         }

                         this.HideProgressIndicator();
                     });

        }


        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            Events<Login>.Dispose(_loginSubsriber);
            Events<Configuration>.Dispose(_confSubsriber);
        }
    }
}
