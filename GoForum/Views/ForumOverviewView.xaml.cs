﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GoForum.Helpers;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Resx;
using GoForum.ViewModels;
using Microsoft.Phone.Controls;

namespace GoForum.Views
{
    public partial class ForumOverviewView : PhoneApplicationPage
    {
        private CategoriesViewModel _categoriesViewModel { get; set; }

        private SubscriptionsViewModel _subscriptionsViewModel { get; set; }

        public ForumOverviewView()
        {
            _categoriesViewModel = IoCContainer.Get<CategoriesViewModel>(App.CurrentForumUrl);
            _subscriptionsViewModel = IoCContainer.Get<SubscriptionsViewModel>(App.CurrentForumUrl);
            InitializeComponent();
            Loaded += ForumOverviewView_Loaded;
        }

        void ForumOverviewView_Loaded(object sender, RoutedEventArgs e)
        {
            _subscriptionsViewModel.Topics.Clear();

            _categoriesViewModel.InitSubsribers();

            if (_categoriesViewModel.Categories.Count == 0)
            {
                var queryString = NavigationContext.QueryString;

                ApplicationTitle.Title = StateHelper.GetFromCurrentState(States.CATEGORIES_TITLE, "").ToUpper();

                if (queryString.Count == 1)
                {
                    int id = Convert.ToInt32(queryString["id"]);

                    _categoriesViewModel.GetForums(id);
                }
                else
                {
                    _categoriesViewModel.GetForums();
                }

                this.ShowProgressIndicator("Getting categories");
            }

            _categoriesViewModel.Categories.CollectionChanged += (ss, ee) => this.HideProgressIndicator();


            _subscriptionsViewModel.InitSubsribers(NavigationService);
            _subscriptionsViewModel.GetSubscriptions();

            SubscriptionsGrid.DataContext = _subscriptionsViewModel;
            CategoriesGrid.DataContext = _categoriesViewModel;
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            _categoriesViewModel.Dispose();
            _subscriptionsViewModel.Dispose();
            base.OnNavigatedFrom(e);
        }

        private void ForumList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (forumList.SelectedIndex == -1)
                return;

            var forum = forumList.SelectedItem as ForumObj;

            if (forum != null)
            {
                var forumStr = string.Format("{0}", forum.ForumId);
                StateHelper.SetToCurrentState(forum.ForumName, States.CATEGORIES_TITLE);
                if (forum.IsSubForumOnly)
                {
                    NavigationService.Navigate(new Uri("/Categories/" + forumStr, UriKind.Relative));
                }
                else
                {
                    NavigationService.Navigate(new Uri("/Topics/" + forumStr, UriKind.Relative));
                }
            }

            forumList.SelectedIndex = -1;
        }

        private void SubscriptionsList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (subsList.SelectedIndex == -1)
                return;

            var topic = subsList.SelectedItem as Topic;

            if (topic != null)
            {
                StateHelper.SetToCurrentState(topic.Title, States.TOPIC_TITLE);
                var uriStr = string.Format("/Posts/{0}", topic.ID);
                NavigationService.Navigate(new Uri(uriStr, UriKind.Relative));
            }

            subsList.SelectedIndex = -1;
        }

        private void unsubsAction_Click(object sender, RoutedEventArgs e)
        {
            Topic topic = (sender as MenuItem).DataContext as Topic;

            try
            {
                _subscriptionsViewModel.TopicService.UnsubscribeTopic(Int32.Parse(topic.ID));
                this.ShowProgressIndicator("Unsubscribing from topic");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.HideProgressIndicator();
            }
        }

        private void TitleContextMenu_Opened(object sender, RoutedEventArgs e)
        { }
    }
}