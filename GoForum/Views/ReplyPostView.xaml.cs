﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Info;
using GoForum.ViewModels;

namespace GoForum.Views
{
    public partial class ReplyPostView : PhoneApplicationPage
    {
        private ReplyPostViewModel ViewModel
        {
            get { return (ReplyPostViewModel)DataContext; }
            set { DataContext = value; }
        }

        public ReplyPostView()
        {
            InitializeComponent();
            ViewModel = IoCContainer.Get<ReplyPostViewModel>(App.CurrentForumUrl);
            Loaded += ReplyPost_Loaded;
        }

        void ReplyPost_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.Load(NavigationContext.QueryString, NavigationService);
            ViewModel.InitSubsribers();           
            Body.Focus();
        }

        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            this.ShowProgressIndicator("Submiting post");
            ViewModel.Submit();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            ViewModel.Dispose();
            base.OnNavigatedFrom(e);
        }
    }
}