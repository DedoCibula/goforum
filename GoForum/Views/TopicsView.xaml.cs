﻿using System;
using System.Windows;
using System.Windows.Controls;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Models.Tapatalk_Structures.InnerStructures;
using GoForum.ViewModels;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using GoForum.Helpers;
using GoForum.Resx;

namespace GoForum.Views
{
    public partial class TopicsView : PhoneApplicationPage
    {
        private TopicsViewModel ViewModel
        {
            get { return (TopicsViewModel)DataContext; }
            set { DataContext = value; }
        }

        public TopicsView()
        {
            InitializeComponent();
            ViewModel = IoCContainer.Get<TopicsViewModel>(App.CurrentForumUrl);
            Loaded += CategoriesView_Loaded;
        }

        protected void CategoriesView_Loaded(object sender, RoutedEventArgs e)
        {          
            ViewModel.InitSubsribers();

            var queryString = NavigationContext.QueryString;

            ApplicationTitle.Text = StateHelper.GetFromCurrentState(States.CATEGORIES_TITLE, "").ToUpper();

            if (queryString.Count >= 1)
            {
                int id = Convert.ToInt32(queryString["id"]);

                if (ViewModel.Topics.Count == 0)
                {
                    ViewModel.GetTopics(id, TopicMode.ANN);
                    ViewModel.GetTopics(id, TopicMode.TOP);
                    ViewModel.GetTopics(id, TopicMode.NORMAL);

                    this.ShowProgressIndicator("Getting topic list");
                }
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            ViewModel.Dispose();
            base.OnNavigatedFrom(e);
        }

        private void ForumList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (forumList.SelectedIndex == -1)
                return;

            var topic = forumList.SelectedItem as Topic;

            if (topic != null)
            {
                StateHelper.SetToCurrentState(topic.Title, States.TOPIC_TITLE);
                var uriStr = string.Format("/Posts/{0}", topic.ID);
                NavigationService.Navigate(new Uri(uriStr, UriKind.Relative));
            }

            forumList.SelectedIndex = -1;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            this.ShowProgressIndicator("Getting topic list");
            ViewModel.GetNextTopics();
        }

        private void NewTopicButton_Click(object sender, EventArgs e)
        {
            var uriString = string.Format("/NewTopic/{0}", ViewModel.ForumID);
            NavigationService.Navigate(new Uri(uriString, UriKind.Relative));
        }

        private void subsAction_Click(object sender, RoutedEventArgs e)
        {
            Topic topic = (sender as MenuItem).DataContext as Topic;

            try
            {
                ViewModel.TopicService.SubscribeTopic(Int32.Parse(topic.ID));
                this.ShowProgressIndicator("Subscribing to topic");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.HideProgressIndicator();
            }
        }

        private void TitleContextMenu_Opened(object sender, RoutedEventArgs e)
        {

        }
    }
}