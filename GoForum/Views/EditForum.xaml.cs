﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using GoForum.Models;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Services;
using GoForum.Services.Forum;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using GoForum.Helpers;

namespace GoForum.Views
{
    public partial class EditForum : PhoneApplicationPage
    {
        private FavoriteForumProvider _favForumProvider;

        private FavoriteForum _favoriteForum;

        private IDisposable _confSubsriber;
        private IDisposable _loginSubsriber;

        public EditForum()
        {
            _favForumProvider = new FavoriteForumProvider();
            InitializeComponent();
            Loaded += EditForum_Loaded;
        }

        void EditForum_Loaded(object sender, RoutedEventArgs e)
        {
            var mobiquoUrl = NavigationContext.QueryString["mobUrl"];
            _favoriteForum = _favForumProvider.FindByMobiquoUrl(mobiquoUrl);
            if (_favoriteForum != null)
            {
                ForumNameTextBox.Text = _favoriteForum.Name;
                UsernameTextBox.Text = _favoriteForum.Username;
                PasswordTextBox.Password = _favoriteForum.Password;
            }
            else
            {
                MessageBox.Show("Error while loading forum!");
                NavigationService.Navigate(new Uri("/Mainpage.xaml", UriKind.RelativeOrAbsolute));
            }

            _confSubsriber = Events<Configuration>.Subscribe(
               cnf => IoCContainer.Get<IUserService>(_favoriteForum.MobiquoUrl).Login(_favoriteForum.Username, _favoriteForum.Password),
               ex =>
               {
               });

            _loginSubsriber = Events<Login>.Subscribe(
                login =>
                {
                    if (login.Result)
                    {
                        _favoriteForum.Name = ForumNameTextBox.Text;
                        _favoriteForum.Username = UsernameTextBox.Text;
                        _favoriteForum.Password = PasswordTextBox.Password;
                        _favForumProvider.Update(_favoriteForum);
                        MessageBox.Show("Forum was successfully edited!");
                        NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.RelativeOrAbsolute));
                    }
                    else
                    {
                        MessageBox.Show(login.ResultText);
                        SignButton.IsEnabled = true;
                    }
                },
                ex =>
                {
                });
        }

        private void SignButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.ShowProgressIndicator("Trying to log in...");

            SignButton.IsEnabled = false;

            IoCContainer.Get<IForumService>(_favoriteForum.MobiquoUrl).GetConfiguration();

            this.HideProgressIndicator();
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            Events<Login>.Dispose(_loginSubsriber);
            Events<Configuration>.Dispose(_confSubsriber);
        }
    }
}
