﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GoForum.Models.Tapatalk_Structures;
using GoForum.ViewModels;
using Microsoft.Phone.Controls;
using GoForum.Helpers;
using GoForum.Resx;

namespace GoForum.Views
{
    public partial class CategoriesView : PhoneApplicationPage
    {
        private CategoriesViewModel ViewModel
        {
            get { return (CategoriesViewModel)DataContext; }
            set { DataContext = value; }
        }

        public CategoriesView()
        {
            InitializeComponent();
            ViewModel = IoCContainer.Get<CategoriesViewModel>(App.CurrentForumUrl);
            Loaded += new RoutedEventHandler(CategoriesView_Loaded);
        }

        protected void CategoriesView_Loaded(object sender, RoutedEventArgs e)
        {          
            ViewModel.InitSubsribers();

            if (ViewModel.Categories.Count == 0)
            {
                var queryString = NavigationContext.QueryString;

                ApplicationTitle.Text = StateHelper.GetFromCurrentState(States.CATEGORIES_TITLE, "").ToUpper();

                if (queryString.Count == 1)
                {      
                    int id = Convert.ToInt32(queryString["id"]);

                    ViewModel.GetForums(id);
                }
                else
                {
                    ViewModel.GetForums();
                }

                this.ShowProgressIndicator("Getting categories");
            }

            ViewModel.Categories.CollectionChanged += (ss, ee) => this.HideProgressIndicator();
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            ViewModel.Dispose();
            base.OnNavigatedFrom(e);
        }

        private void ForumList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (forumList.SelectedIndex == -1)
                return;

            var forum = forumList.SelectedItem as ForumObj;

            if (forum != null)
            {
                var forumStr = string.Format("{0}", forum.ForumId);
                StateHelper.SetToCurrentState(forum.ForumName, States.CATEGORIES_TITLE);
                if (forum.IsSubForumOnly)
                {
                    NavigationService.Navigate(new Uri("/Categories/" + forumStr, UriKind.Relative));
                }
                else
                {
                    NavigationService.Navigate(new Uri("/Topics/" + forumStr, UriKind.Relative));
                }
            }

            forumList.SelectedIndex = -1;
        }
    }
}