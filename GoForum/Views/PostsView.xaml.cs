﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GoForum.Models.Tapatalk_Structures;
using GoForum.ViewModels;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using GoForum.Helpers;
using GoForum.Resx;

namespace GoForum.Views
{
    public partial class PostsView : PhoneApplicationPage
    {
        #region Properties

        private PostsViewModel ViewModel
        {
            get { return (PostsViewModel)DataContext; }
            set { DataContext = value; }
        }

        private int TopicID { get; set; }

        #endregion

        public PostsView()
        {
            InitializeComponent();
            ViewModel = IoCContainer.Get<PostsViewModel>(App.CurrentForumUrl);
            Loaded += PostsView_Loaded;
        }

        protected void PostsView_Loaded(object sender, RoutedEventArgs e)
        {          
            ViewModel.InitSubsribers();

            var queryString = NavigationContext.QueryString;

            if (queryString.Count >= 1)
            {
                ApplicationTitle.Text = StateHelper.GetFromCurrentState(States.TOPIC_TITLE, "").ToUpper();
                
                TopicID = Convert.ToInt32(queryString["id"]);

                StateHelper.SetToCurrentState(TopicID, States.TOPIC_ID);

                this.ShowProgressIndicator("Getting posts");
                ViewModel.GetThread(TopicID, 0, 9);
            }

            ViewModel.Posts.CollectionChanged += (s, ee) => 
            { 
                InitPagingButtons(); 
                this.HideProgressIndicator();
            };
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            ViewModel.Dispose();
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (NavigationService.BackStack.First().Source.ToString().Contains("NewTopic") || NavigationService.BackStack.First().Source.ToString().Contains("ReplyPost"))
            {
                NavigationService.RemoveBackEntry();
            }



            base.OnNavigatedTo(e);
        }

        #region Event hanlders

        private void FirstPageButton_Click(object sender, EventArgs e)
        {
            ViewModel.Paginator.First();
            GetTopics();
        }
        
        private void PreviousIconButton_Click(object sender, EventArgs e)
        {
            ViewModel.Paginator.Previous();
            GetTopics();
        }

        private void NextIconButton_Click(object sender, EventArgs e)
        {
            ViewModel.Paginator.Next();
            GetTopics();
        }

        private void LastPageButton_Click(object sender, EventArgs e)
        {
            ViewModel.Paginator.Last();
            GetTopics();
        }

        #endregion

        private void GetTopics()
        {
            this.ShowProgressIndicator("Getting posts");
            ViewModel.GetThread(TopicID, ViewModel.Paginator.Start, ViewModel.Paginator.End);
        }

        private void InitPagingButtons()
        {
            var firstPageButton = ApplicationBar.Buttons[0] as ApplicationBarIconButton;
            var previousPageButton = ApplicationBar.Buttons[1] as ApplicationBarIconButton;
            var nextPageButton = ApplicationBar.Buttons[2] as ApplicationBarIconButton;
            var lastPageButton = ApplicationBar.Buttons[3] as ApplicationBarIconButton;
            previousPageButton.IsEnabled = firstPageButton.IsEnabled = !ViewModel.Paginator.IsFirst;
            nextPageButton.IsEnabled = lastPageButton.IsEnabled = !ViewModel.Paginator.IsLast;
        }

        private void PostContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            Post post = (sender as ContextMenu).DataContext as Post;
        }

        private void replyMenuAction_Click(object sender, RoutedEventArgs e)
        {
            Post post = (sender as MenuItem).DataContext as Post;
            ReplyPost(post);
        }

        private void ReplyPostButton_Click(object sender, EventArgs e)
        {
            Post post = ViewModel.Posts.Last();
            ReplyPost(post);
        }

        private void ReplyPost(Post post)
        {
            StateHelper.SetToCurrentState(post.Title, States.REPLYPOST_SUBJECT);
            var uriStr = string.Format("/ReplyPost");

            App.CurrentThread = ViewModel.CurrentThread;

            NavigationService.Navigate(new Uri(uriStr, UriKind.Relative));
        }
    }
}