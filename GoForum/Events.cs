﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;


namespace GoForum
{
    // to be extended
    public static class Events<T> where T: class
    {
        private static GoSubject<T> _observableEvent;
        private static GoSubject<T> ObservableEvent
        {
            get 
            {
                _observableEvent = _observableEvent ?? (_observableEvent = new GoSubject<T>());
                return _observableEvent;
            }
        } 

        private static readonly ConcurrentOrderedList<IDisposable> _observers = new ConcurrentOrderedList<IDisposable>();


        /// <summary>
        /// Subscribe using custom scheduler.
        /// </summary>
        public static IDisposable Subscribe(Action<T> onNext, IScheduler scheduler)
        {
            var observer = ObservableEvent.ObserveOn(scheduler).Subscribe(onNext);
            _observers.TryAdd(observer);
            return observer;
        }

        /// <summary>
        /// Subscribe on dispatcher.
        /// </summary>
        public static IDisposable Subscribe(Action<T> onNext)
        {
            return Subscribe(onNext, DispatcherScheduler.Current);
        }


        /// <summary>
        /// Subscribe using custom scheduler.
        /// </summary>
        public static IDisposable Subscribe(Action<T> onNext, Action<Exception> onError, IScheduler scheduler)
        {
            var observer = ObservableEvent.ObserveOn(scheduler).Subscribe(onNext, onError);
            _observers.TryAdd(observer);
            return observer;
        }


        /// <summary>
        /// Subscribe on dispatcher.
        /// </summary>
        public static IDisposable Subscribe(Action<T> onNext, Action<Exception> onError)
        {
            return Subscribe(onNext, onError, DispatcherScheduler.Current);
        }


        /// <summary>
        /// Subscribe using custom scheduler.
        /// </summary>
        public static IDisposable Subscribe(Action<T> onNext, Action onCompleted, IScheduler scheduler)
        {
            var observer = ObservableEvent.ObserveOn(scheduler).Subscribe(onNext, onCompleted);
            _observers.TryAdd(observer);
            return observer;
        }

        /// <summary>
        /// Subscribe on Dispatcher.
        /// </summary>
        public static IDisposable Subscribe(Action<T> onNext, Action onCompleted)
        {
            return Subscribe(onNext, onCompleted, DispatcherScheduler.Current);
        }


        /// <summary>
        /// Subscribe using custom scheduler.
        /// </summary>
        public static IDisposable Subscribe(Action<T> onNext, Action<Exception> onError, Action onCompleted, IScheduler scheduler)
        {
            var observer = ObservableEvent.ObserveOn(scheduler).Subscribe(onNext, onError, onCompleted);
            _observers.TryAdd(observer);
            return observer;
        }


        /// <summary>
        /// Subscribe on Dispatcher.
        /// </summary>
        public static IDisposable Subscribe(Action<T> onNext, Action<Exception> onError, Action onCompleted)
        {
            return Subscribe(onNext, onError, onCompleted, DispatcherScheduler.Current);
        }


        public static void OnNext(T type)
        {
            ObservableEvent.OnNext(type);
        }

        public static void OnError(Exception exception)
        {
            ObservableEvent.OnError(exception);
        }

        public static void OnCompleted()
        {
            ObservableEvent.OnCompleted();
        }

        public static void DisposeAll()
        {
            foreach (var observer in _observers)
            {
                observer.Dispose();
                _observers.TryRemove(observer);
            }
        }

        public static void Dispose(IEnumerable<IDisposable> disposables)
        {
            Dispose(disposables.ToArray());
        }

        public static void Dispose(params IDisposable[] disposables)
        {
            foreach (var disposable in disposables)
            {
                if (_observers.Contains(disposable))
                {
                    disposable.Dispose();
                    _observers.TryRemove(disposable);
                }
            }
        }
    }
}
