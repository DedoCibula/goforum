﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GoForum
{
    public class TaskMonitor
    {
        private Task _runningTask;
        private volatile object _privateLock = new object();

        public void AddToQueue(Func<IAsyncResult> method, Action<IAsyncResult> callback, TimeSpan timeout)
        {
            lock (_privateLock)
            {
                if (_runningTask != null)
                {
                    Task.WaitAny(new[] { _runningTask }, timeout);
                }
                _runningTask = Task.Factory.FromAsync(method(), callback);
            }
        }
    }
}
