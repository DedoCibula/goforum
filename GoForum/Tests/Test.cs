﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace GoForum.Tests
{
    public class Test
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        private readonly Func<bool> _assertion;

        public bool IsOk { get; private set; }

        public Test(string name, string description, Func<bool> function)
        {
            Name = name;
            Description = description;
            _assertion = function;
        }

        public void Run()
        {
            IsOk = _assertion();
        }
    }
}
