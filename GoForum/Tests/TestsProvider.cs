﻿using GoForum.Services.Forum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoForum.Tests
{
    public class TestsProvider
    {
        public static IEnumerable<Test> GetTests()
        {
            IList<Test> tests = new List<Test>();

            tests.Add(new Test("Tapatalk Plugin Available", "pv239.pavelkouril.cz", () =>
            {
                MobiquoDiscovery discovery = new MobiquoDiscovery();
                return discovery.CheckIfForumHasPlugin("pv239.pavelkouril.cz");
            }));

            tests.Add(new Test("Tapatalk Plugin Available", "pojo.biz", () =>
            {
                MobiquoDiscovery discovery = new MobiquoDiscovery();
                return discovery.CheckIfForumHasPlugin("pojo.biz");
            }));

            tests.Add(new Test("Tapatalk Plugin Available", "pojo.biz/board", () =>
            {
                MobiquoDiscovery discovery = new MobiquoDiscovery();
                return discovery.CheckIfForumHasPlugin("pojo.biz/board");
            }));

            tests.Add(new Test("Tapatalk Plugin Available", "nonexistentdomain.abcd", () =>
            {
                MobiquoDiscovery discovery = new MobiquoDiscovery();
                return !discovery.CheckIfForumHasPlugin("nonexistentdomain.abcd");
            }));

            tests.Add(new Test("Tapatalk Plugin Available", "forum.xda-developers.com", () =>
            {
                MobiquoDiscovery discovery = new MobiquoDiscovery();
                return discovery.CheckIfForumHasPlugin("forum.xda-developers.com");
            }));

            tests.Add(new Test("Tapatalk Plugin Available", "https://forum.xda-developers.com", () =>
            {
                MobiquoDiscovery discovery = new MobiquoDiscovery();
                return discovery.CheckIfForumHasPlugin("https://forum.xda-developers.com");
            }));

            tests.Add(new Test("Tapatalk Plugin Available", "pv239.pavelkouril.cz/abc.php", () =>
            {
                MobiquoDiscovery discovery = new MobiquoDiscovery();
                return discovery.CheckIfForumHasPlugin("pv239.pavelkouril.cz/abc.php");
            }));

            return tests;
        }
    }
}
