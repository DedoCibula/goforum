﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using GoForum.Models.Tapatalk_Structures;
using GoForum.Services;
using GoForum.Services.Tapatalk_API;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Threading;

namespace GoForum.Tests
{
    public partial class TestPage : PhoneApplicationPage
    {
        private readonly ObservableCollection<Test> _tests = new ObservableCollection<Test>();
        public ObservableCollection<Test> Tests
        {
            get { return _tests; }
        }

        public TestPage()
        {
            InitializeComponent();
            TestsListBox.ItemsSource = Tests;
            Loaded += TestPage_Loaded;
        }

        void TestPage_Loaded(object sender, RoutedEventArgs eargs)
        {
            #region Services tests

            var forumService = IoCContainer.Get<IForumService>("http://pv239.pavelkouril.cz/mobiquo/mobiquo.php");

            var userService = IoCContainer.Get<IUserService>("http://pv239.pavelkouril.cz/mobiquo/mobiquo.php");

            var postService = IoCContainer.Get<IPostService>("http://pv239.pavelkouril.cz/mobiquo/mobiquo.php");

            #region Callbacks

            Events<Configuration>.Subscribe(c =>
            {
                var config = c;

                userService.Login("admin", "heslo123");

                // HC testing
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //forumService.GetForums();
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //forumService.GetForums();
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //forumService.GetForums();
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //forumService.GetForums();
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //forumService.GetForums();
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //forumService.GetForums();
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //forumService.GetForums();
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
                //postService.ReplyPost(2, 9, "Dafuq", "This is Sparta");
            }, e =>
            {
                var exception = e;
            });

            Events<ForumObj[]>.Subscribe(c =>
            {
                var config = c;
            }, e =>
            {
                var exception = e;
            });

            Events<LoginForum>.Subscribe(c =>
            {
                var config = c;
            }, e =>
            {
                var exception = e;
            });

            Events<BoardStats>.Subscribe(c =>
            {
                var config = c;
            }, e =>
            {
                var exception = e;
            });

            Events<ForumStatus>.Subscribe(c =>
            {
                var config = c;
            }, e =>
            {
                var exception = e;
            });

            Events<MarkAllAsRead>.Subscribe(c =>
            {
                var config = c;
            }, e =>
            {
                var exception = e;
            });




            #endregion

            Events<Login>.Subscribe(c =>
            {
                var config = c;
            }, e =>
            {
                var exception = e;
            });



            Events<Logout>.Subscribe(c =>
            {
                var config = c;
            }, e =>
            {
                var exception = e;
            });

            Events<ReplyPost>.Subscribe(c =>
            {
                var config = c;
            }, e =>
            {
                var exception = e;
            });

            // WORKS
            forumService.GetConfiguration();

            // WORKS
            //forumService.GetForums();

            // CAN'T BE TESTED - NO DATA
            //forumService.LoginForum(1, "heslo123");

            // WORKS
            //forumService.GetBoardStatistics();

            // WORKS
            //forumService.MarkAllAsRead(50);

            // CAN'T BE TESTED
            //forumService.GetForumStatus(1);



            #endregion

            #region Tapatalk API tests

            TapatalkProxy proxy = new TapatalkProxy();
            proxy.Url = "http://pv239.pavelkouril.cz/mobiquo/mobiquo.php";

            //WORKS
            //proxy.BeginGetConfiguration(result =>
            //{
            //    var config = proxy.EndGetConfiguration(result);

            //    // aby config nesmazal optimalizer 
            //    var config2 = config;
            //});

            //WORKS
            //proxy.BeginGetForum(true, null, result =>
            //{
            //    var config = proxy.EndGetForum(result);

            //    Dispatcher.BeginInvoke(() =>
            //        {
            //            forumList.ItemsSource = config;
            //        });
            //});

            //WORKS
            //proxy.BeginLoginForum(1, null, result =>
            //{
            //    var config = proxy.EndLoginForum(result);

            //    var config2 = config;
            //});

            //WORKS
            //proxy.BeginGetBoardStatistics(result =>
            //{
            //    var config = proxy.EndGetBoardStatistics(result);

            //    var config2 = config;
            //});

            //WORKS
            //proxy.BeginMarkAllAsRead(1, result =>
            //{
            //    var config = proxy.EndMarkAllAsRead(result);

            //    var config2 = config;
            //});

            //CAN'T BE TESTED
            //proxy.BeginGetParticipatedForum(result =>
            //{
            //    var config = proxy.EndGetParticipatedForum(result);

            //    var config2 = config;
            //});

            //DOESN'T WORK WITH pajousek AND tapatalk forums
            //proxy.BeginGetForumStatus(new int[] { 1 }, result =>
            //{
            //    var config = proxy.EndGetForumStatus(result);

            //    var config2 = config;
            //});


            // Finally work (I must have used monitor to synchronize responses).
            //proxy.BeginLogin("admin", "heslo123", false, false, result =>
            //    {
            //        try
            //        {
            //            Login loginResult = proxy.EndLogin(result);

            //            proxy.BeginNewTopic(2, "Topic title", "Topic text", null, null, null, res1 =>
            //                {
            //                    try
            //                    {
            //                        var oo = proxy.EndNewTopic(res1);

            //                        var config2 = oo;
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        var s = ex;
            //                        throw;
            //                    }
            //                });



            //            //proxy.BeginReplyPost(2, 9, "Dafuq", "This is Sparta", null, null, null, res =>
            //            //{
            //            //    try
            //            //    {
            //            //        var config33 = proxy.EndReplyPost(res);

            //            //        var config2 = config33;
            //            //    }
            //            //    catch (Exception ex)
            //            //    {
            //            //        var s = ex;
            //            //        throw;
            //            //    }

            //            //});

            //            //proxy.BeginGetForum(true, null, www =>
            //            //{
            //            //    try
            //            //    {
            //            //        var config = proxy.EndGetForum(www);
            //            //        Dispatcher.BeginInvoke(() =>
            //            //        {
            //            //            forumList.ItemsSource = config;
            //            //        });

            //            //        proxy.SetCookies(App.Cookies[proxy.Url]);

            //            //    }
            //            //    catch (Exception ex)
            //            //    {
            //            //        var s = ex;
            //            //        throw;
            //            //    }
            //            //});

            //        }
            //        catch (Exception ex)
            //        {
            //            var s = ex;
            //        }



            //    });


            //WORKS
            //proxy.BeginLogout(result =>
            //{
            //    var config = proxy.EndLogout(result);

            //    var config2 = config;
            //});

            //WORKS
            //proxy.BeginGetTopic(2, 0, 10, null, result =>
            //    {
            //        var config = proxy.EndGetTopic(result);

            //        var config2 = config;
            //    });

            //WORKS
            //            proxy.BeginGetThread(9, 0, 50, null, result =>
            //                {
            //                    try
            //                    {
            //                        var config = proxy.EndGetThread(result);
            //
            //                        Dispatcher.BeginInvoke(() =>
            //                           {
            //                               forumList.ItemsSource = config.Posts;
            //                           });
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        var s = ex;
            //                        throw;
            //                    }
            //
            //                });

            #endregion


            foreach (var t in TestsProvider.GetTests())
            {
                Task.Run(() =>
                {
                    var test = t;
                    test.Run();
                    Dispatcher.BeginInvoke(() => Tests.Add(test));
                });
            }
        }
    }
}