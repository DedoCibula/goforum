﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace GoForum
{
    public class TimeoutMonitor
    {
        private volatile object _privateLock = new object();
        private bool _hasEntered;
        private Task _cancelationTask;
        private int _lockCount;

        public void Acquire()
        {
            if (!_hasEntered)
            {
                lock (_privateLock)
                {
                    _hasEntered = true;
                    _lockCount++;
                }
            }
            else
            {
                Thread.Sleep(50);
                Acquire();
            }
        }

        
        /// <summary>
        /// Acquires lock.
        /// </summary>
        /// <param name="setTimeOutToAutoRelease">Timeout to autoRelease (suppose web service does not response in time)</param>
        public void Acquire(TimeSpan setTimeOutToAutoRelease)
        {
            if (!_hasEntered)
            {
                lock (_privateLock)
                {
                    _hasEntered = true;
                    _lockCount++;
                    _cancelationTask = new Task(() =>
                        {
                            var s = _lockCount;
                            Thread.Sleep((int)setTimeOutToAutoRelease.TotalMilliseconds);
                            Release(s);
                        });

                    _cancelationTask.Start();
                }
            }
            else
            {
                // dummy wait, anything better?
                Thread.Sleep(50);
                Acquire(setTimeOutToAutoRelease);
            }
        }


        public void Release()
        {
            lock (_privateLock)
            {
                _hasEntered = false;
            }
        }


        private void Release(int i)
        {
            if (i == _lockCount)
            {
                lock (_privateLock)
                {
                    if (i == _lockCount)
                    {
                        _hasEntered = false;
                    }
                }
            }
        }
    }
}
