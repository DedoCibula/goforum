﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Linq;

using GoForum.Models.Tapatalk_Structures;

namespace GoForum.Repositories
{
    public class CookieRepository
    {
        private readonly IDictionary<string, DoubleCookies> _authorizationCookies;
        private volatile object Locker = new object();

        public CookieRepository()
        {
            _authorizationCookies = new ConcurrentDictionary<string, DoubleCookies>();
        }

        public void SaveCookies(string cookieHeader, string cookieUrl)
        {
            lock (Locker)
            {
                var doubleCookies = new DoubleCookies
                {
                    EmptyCookies = cookieHeader.ParseCookies(true),
                    ValidCookies = cookieHeader.ParseCookies()
                };

                if (_authorizationCookies.ContainsKey(cookieUrl))
                {
                    _authorizationCookies[cookieUrl] = doubleCookies;
                }
                else
                {
                    _authorizationCookies.Add(cookieUrl, doubleCookies);
                }
            }
        }


        public void InvalidateCookies(string cookieUrl)
        {
            lock (Locker)
            {
                if (_authorizationCookies.ContainsKey(cookieUrl))
                {
                    var cookies = _authorizationCookies[cookieUrl];

                    cookies.ValidCookies = cookies.EmptyCookies;
                }
            }
        }


        public bool CheckValidity(string url)
        {
            if (string.IsNullOrEmpty(url) || !_authorizationCookies.ContainsKey(url))
                return false;
            var cookies = _authorizationCookies[url].ValidCookies.Where(c => c.Name.StartsWith("php")).ToList();

            return cookies.All(c => c.Expires > DateTime.Now && !string.IsNullOrEmpty(c.Value));
        }


        /// <summary>
        /// Returns cookies based on Url.
        /// </summary>
        /// <param name="url">Url</param>
        /// <param name="emptyCookies">If true, same cookies with empty values will be returned</param>
        public Cookie[] this[string url, bool emptyCookies = false]
        {
            get
            {
                if (_authorizationCookies.ContainsKey(url))
                {
                    return emptyCookies ? _authorizationCookies[url].EmptyCookies : _authorizationCookies[url].ValidCookies;
                }

                return new Cookie[0];
            }
        }
    }


    internal class DoubleCookies
    {
        public Cookie[] ValidCookies { get; set; }
        public Cookie[] EmptyCookies { get; set; }
    }
}
