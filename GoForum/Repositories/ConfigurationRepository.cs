﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

using GoForum.Models.Tapatalk_Structures;
using System.Windows;

namespace GoForum.Repositories
{
    public class ConfigurationRepository : IDisposable
    {
        private readonly IDictionary<string, Configuration> _forumConfiguration;
        private readonly IDisposable _observer;

        public ConfigurationRepository()
        {
            _forumConfiguration = new ConcurrentDictionary<string, Configuration>();
            _observer = Events<Configuration>.Subscribe(c =>
                {
                    if (!string.IsNullOrEmpty(c.Url))
                        _forumConfiguration[c.Url] = c;
                },
                ex =>
                {
                });
        }

        public Configuration this[string url]
        {
            get
            {
                return _forumConfiguration.ContainsKey(url) ? _forumConfiguration[url] : null;
            }
        }

        public void Dispose()
        {
            Events<Configuration>.Dispose(_observer);
        }
    }
}
