﻿using GoForum.Services;
using GoForum.Services.Tapatalk_API;

using Ninject;
using Ninject.Parameters;

namespace GoForum
{
    public static class IoCContainer
    {
        private static readonly IKernel Kernel = new StandardKernel();

        public static void Initialize()
        {
            Kernel.Bind<Paginator>().ToSelf();
            Kernel.Bind<TapatalkProxy>().ToSelf();
            Kernel.Bind<TimeoutMonitor>().ToSelf().InSingletonScope();
            Kernel.Bind<IForumService>().To<ForumService>();
            Kernel.Bind<IUserService>().To<UserService>();
            Kernel.Bind<IPostService>().To<PostService>();
            Kernel.Bind<ITopicService>().To<TopicService>();
        }

        public static T Get<T>()
        {
            return Kernel.Get<T>();
        }

        public static T Get<T>(string url)
        {
            return Kernel.Get<T>(new ConstructorArgument("url", url));
        }
    }
}
