﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GoForum
{
    public class TaskQueue
    {
        private volatile object _privateLock = new object();
        private bool _busy;
        private int _lockCount;
        private readonly ConcurrentQueue<Action> _concurrentQueue;
        private readonly TimeSpan _timeOutToAutoRelease;

        public TaskQueue(TimeSpan timeoutToAutoRelease = default(TimeSpan))
        {
            _concurrentQueue = new ConcurrentQueue<Action>();
            _timeOutToAutoRelease = timeoutToAutoRelease;
        }

        public void Enqueue(Action action)
        {
            _concurrentQueue.Enqueue(action);
            lock (_privateLock)
            {
                if (!_busy)
                {
                    _busy = true;
                    RunAction(_timeOutToAutoRelease);
                }
            }
        }


        public void Dequeue(int? i = null)
        {
            lock (_privateLock)
            {
                if (!_concurrentQueue.IsEmpty)
                {
                    if (!i.HasValue || i == _lockCount)
                    {
                        RunAction(_timeOutToAutoRelease);
                    }

                }
                else
                {
                    _busy = false;
                }
            }
        }

        private void RunAction(TimeSpan setTimeOutToAutoRelease)
        {
            _lockCount++;
            Action currentAction;
            _concurrentQueue.TryDequeue(out currentAction);
            Task.Factory.StartNew(() =>
                {
                    if (setTimeOutToAutoRelease != default(TimeSpan))
                    {
                        Thread.Sleep((int)setTimeOutToAutoRelease.TotalMilliseconds);
                        Dequeue(_lockCount);
                    }
                });
            currentAction();
        }
    }
}
