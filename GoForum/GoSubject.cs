﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Subjects;
using System.Threading;

namespace GoForum
{
    /// <summary>
    /// Represents an object that is both an observable sequence as well as an observer.
    ///             Each notification is broadcasted to all subscribed observers.
    /// 
    /// </summary>
    /// <typeparam name="T">The type of the elements processed by the subject.</typeparam>
    public sealed class GoSubject<T> : ISubject<T>, ISubject<T, T>, IObserver<T>, IObservable<T>, IDisposable
    {
        private volatile IObserver<T> _observer;

        /// <summary>
        /// Indicates whether the subject has observers subscribed to it.
        /// 
        /// </summary>
        public bool HasObservers
        {
            get
            {
                if (this._observer != NopObserver<T>.Instance && !(this._observer is DoneObserver<T>))
                    return this._observer != DisposedObserver<T>.Instance;
                else
                    return false;
            }
        }

        /// <summary>
        /// Creates a subject.
        /// 
        /// </summary>
        public GoSubject()
        {
            this._observer = NopObserver<T>.Instance;
        }

        /// <summary>
        /// Notifies all subscribed observers about the end of the sequence.
        /// 
        /// </summary>
        public void OnCompleted()
        {
            IObserver<T> observer = DoneObserver<T>.Completed;
            IObserver<T> comparand;
            do
            {
                comparand = this._observer;
            }
            while (comparand != DisposedObserver<T>.Instance && !(comparand is DoneObserver<T>) && Interlocked.CompareExchange<IObserver<T>>(ref this._observer, observer, comparand) != comparand);
            comparand.OnCompleted();
        }

        /// <summary>
        /// Notifies all subscribed observers about the specified exception.
        /// 
        /// </summary>
        /// <param name="error">The exception to send to all currently subscribed observers.</param><exception cref="T:System.ArgumentNullException"><paramref name="error"/> is null.</exception>
        public void OnError(Exception error)
        {
            if (error == null)
                throw new ArgumentNullException("error");
            _observer.OnError(error);
        }

        /// <summary>
        /// Notifies all subscribed observers about the arrival of the specified element in the sequence.
        /// 
        /// </summary>
        /// <param name="value">The value to send to all currently subscribed observers.</param>
        public void OnNext(T value)
        {
            this._observer.OnNext(value);
        }

        /// <summary>
        /// Subscribes an observer to the subject.
        /// 
        /// </summary>
        /// <param name="observer">Observer to subscribe to the subject.</param>
        /// <returns>
        /// Disposable object that can be used to unsubscribe the observer from the subject.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException"><paramref name="observer"/> is null.</exception>
        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (observer == null)
                throw new ArgumentNullException("observer");
            IObserver<T> comparand;
            IObserver<T> observer1;
            do
            {
                comparand = this._observer;
                if (comparand == DisposedObserver<T>.Instance)
                    throw new ObjectDisposedException("");
                if (comparand == DoneObserver<T>.Completed)
                {
                    observer.OnCompleted();
                    return Disposable.Empty;
                }
                else
                {
                    DoneObserver<T> doneObserver = comparand as DoneObserver<T>;
                    if (doneObserver != null)
                    {
                        observer.OnError(doneObserver.Exception);
                        return Disposable.Empty;
                    }
                    else if (comparand == NopObserver<T>.Instance)
                    {
                        observer1 = observer;
                    }
                    else
                    {
                        Observer<T> observer2 = comparand as Observer<T>;
                        if (observer2 != null)
                            observer1 = observer2.Add(observer);
                        else
                            observer1 = (IObserver<T>)new Observer<T>(new ImmutableList<IObserver<T>>(new IObserver<T>[2]
              {
                comparand,
                observer
              }));
                    }
                }
            }
            while (Interlocked.CompareExchange<IObserver<T>>(ref this._observer, observer1, comparand) != comparand);
            return (IDisposable)new GoSubject<T>.Subscription(this, observer);
        }

        private void Unsubscribe(IObserver<T> observer)
        {
            IObserver<T> comparand;
            IObserver<T> observer1;
            do
            {
                comparand = this._observer;
                if (comparand == DisposedObserver<T>.Instance || comparand is DoneObserver<T>)
                    break;
                Observer<T> observer2 = comparand as Observer<T>;
                if (observer2 != null)
                {
                    observer1 = observer2.Remove(observer);
                }
                else
                {
                    if (comparand != observer)
                        break;
                    observer1 = NopObserver<T>.Instance;
                }
            }
            while (Interlocked.CompareExchange<IObserver<T>>(ref this._observer, observer1, comparand) != comparand);
        }

        /// <summary>
        /// Releases all resources used by the current instance of the <see cref="T:System.Reactive.Subjects.Subject`1"/> class and unsubscribes all observers.
        /// 
        /// </summary>
        public void Dispose()
        {
            this._observer = DisposedObserver<T>.Instance;
        }

        private class Subscription : IDisposable
        {
            private GoSubject<T> _subject;
            private IObserver<T> _observer;

            public Subscription(GoSubject<T> subject, IObserver<T> observer)
            {
                this._subject = subject;
                this._observer = observer;
            }

            public void Dispose()
            {
                IObserver<T> observer = Interlocked.Exchange<IObserver<T>>(ref this._observer, (IObserver<T>)null);
                if (observer == null)
                    return;
                this._subject.Unsubscribe(observer);
                this._subject = (GoSubject<T>)null;
            }
        }
    }

    internal class NopObserver<T> : IObserver<T>
    {
        public static readonly IObserver<T> Instance = (IObserver<T>)new NopObserver<T>();

        static NopObserver()
        {
        }

        public void OnCompleted()
        {
        }

        public void OnError(Exception error)
        {
        }

        public void OnNext(T value)
        {
        }
    }

    internal class DisposedObserver<T> : IObserver<T>
    {
        public static readonly IObserver<T> Instance = (IObserver<T>)new DisposedObserver<T>();

        static DisposedObserver()
        {
        }

        public void OnCompleted()
        {
            throw new ObjectDisposedException("");
        }

        public void OnError(Exception error)
        {
            throw new ObjectDisposedException("");
        }

        public void OnNext(T value)
        {
            throw new ObjectDisposedException("");
        }
    }

    internal class Observer<T> : IObserver<T>
    {
        private readonly ImmutableList<IObserver<T>> _observers;

        public Observer(ImmutableList<IObserver<T>> observers)
        {
            this._observers = observers;
        }

        public void OnCompleted()
        {
            foreach (IObserver<T> observer in this._observers.Data)
                observer.OnCompleted();
        }

        public void OnError(Exception error)
        {
            foreach (IObserver<T> observer in this._observers.Data)
                observer.OnError(error);
        }

        public void OnNext(T value)
        {
            foreach (IObserver<T> observer in this._observers.Data)
                observer.OnNext(value);
        }

        internal IObserver<T> Add(IObserver<T> observer)
        {
            return (IObserver<T>)new Observer<T>(this._observers.Add(observer));
        }

        internal IObserver<T> Remove(IObserver<T> observer)
        {
            int num = Array.IndexOf<IObserver<T>>(this._observers.Data, observer);
            if (num < 0)
                return (IObserver<T>)this;
            if (this._observers.Data.Length == 2)
                return this._observers.Data[1 - num];
            else
                return (IObserver<T>)new Observer<T>(this._observers.Remove(observer));
        }
    }

    internal class ImmutableList<T>
    {
        private T[] data;

        public T[] Data
        {
            get
            {
                return this.data;
            }
        }

        public ImmutableList()
        {
            this.data = new T[0];
        }

        public ImmutableList(T[] data)
        {
            this.data = data;
        }

        public ImmutableList<T> Add(T value)
        {
            T[] data = new T[this.data.Length + 1];
            Array.Copy((Array)this.data, (Array)data, this.data.Length);
            data[this.data.Length] = value;
            return new ImmutableList<T>(data);
        }

        public ImmutableList<T> Remove(T value)
        {
            int num = this.IndexOf(value);
            if (num < 0)
                return this;
            T[] data = new T[this.data.Length - 1];
            Array.Copy((Array)this.data, 0, (Array)data, 0, num);
            Array.Copy((Array)this.data, num + 1, (Array)data, num, this.data.Length - num - 1);
            return new ImmutableList<T>(data);
        }

        public int IndexOf(T value)
        {
            for (int index = 0; index < this.data.Length; ++index)
            {
                if (this.data[index].Equals((object)value))
                    return index;
            }
            return -1;
        }
    }

    internal class DoneObserver<T> : IObserver<T>
    {
        public static readonly IObserver<T> Completed = (IObserver<T>)new DoneObserver<T>();

        public Exception Exception { get; set; }

        static DoneObserver()
        {
        }

        public void OnCompleted()
        {
        }

        public void OnError(Exception error)
        {
        }

        public void OnNext(T value)
        {
        }
    }
}
