# GoForum #

Windows Phone mobile client for one of the most popular forum apps - Tapatalk.

### Technologies and Patterns ###

* Windows Phone SDK 7.1
* Windows Phone Toolkit
* XML-RPC.NET
* NInject
* Reactive Extensions (Rx)
* MVVM, IoC and Observer Design Patterns